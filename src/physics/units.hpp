#ifndef PHYSICS_UNITS_HPP
#define PHYSICS_UNITS_HPP

namespace Physics
{

typedef double Kelvins;
typedef double Meters;
typedef double SquareMeters;
typedef double CubicMeters;
typedef double Pascals;
typedef double Moles;
typedef double Joules;
typedef double Watts;
typedef double NewtonMeters;
typedef double RadiansPerSecond;
typedef double Radians;
typedef double Seconds;

}

#endif
