#ifndef PHYSICS_PROCESS_HPP
#define PHYSICS_PROCESS_HPP

#include <utility>
#include <vector>

#include "units.hpp"

namespace Physics
{

typedef std::vector<std::pair<CubicMeters, Pascals>> PressureVolumeGraph;
typedef std::vector<std::pair<Radians, double>> MassFlowGraph;

class Process
{
	public:
		class State
		{
			public:
				Kelvins temperature;
				CubicMeters volume;
				Pascals pressure;

				double calculateSubstanceAmount() const;
		};

		Joules getWork(RadiansPerSecond engineSpeed);
		PressureVolumeGraph getPvGraph(RadiansPerSecond engineSpeed);

		Process(State beginningState, double compressionVolume, double crankRadius, double rodLength, double bore);

		double calculateTrapezoidArea(double h, double b, double a);
		Process::State addHeat(const Process::State& state, double heatAddition);
	private:
		State compressAdiabatically(const State& state, double newVolume);

		const State beginningState;
		const double gasConstant;
		const double compressionVolume;
		const double crankRadius;
		const double rodLength;
		const double bore;
};

};

#endif // GAS_HPP
