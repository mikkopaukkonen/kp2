#include "process.hpp"

#include <cmath>
#include <iostream>
#include "math_tools.hpp"

namespace Physics
{

const double R = 8.3144621;

double getAreaInPolygon(PressureVolumeGraph& graph) {
	double area = 0.0;

	PressureVolumeGraph::size_type j = graph.size() - 1;
	for (PressureVolumeGraph::size_type i = 0; i != graph.size(); i++)
	{
		PressureVolumeGraph::value_type point;

		area += (graph[j].second + graph[i].second) * (graph[j].first - graph[i].first);
		j = i;
	}

	return abs(area / 2.0);
}

double getVolume(Radians angle, CubicMeters clearanceVolume, Meters crankRadius, Meters rodLength, Meters bore) {
	double s = crankRadius * cos(angle) + sqrt(pow(rodLength, 2.0) - pow(crankRadius, 2.0) * pow(sin(angle), 2.0));
	double v = clearanceVolume + (PI * std::pow(bore/2.0, 2.0) * (rodLength + crankRadius - s));
	return v;
}

PressureVolumeGraph Process::getPvGraph(RadiansPerSecond engineSpeed)
{
	const double TOTAL_RADIANS = /*2.0 **/ 2.0 * PI;
	const int ANGLES = /*2 **/ 2 * 30;

	bool addedHeat = false;

	PressureVolumeGraph graph;
	State start = beginningState;
	State state = start;

	MassFlowGraph massFlowGraph;

	// The last angle would be the first angle of the next cycle
	for(int i = 0; i < ANGLES - 1; ++i) {
		double angle = TOTAL_RADIANS / double(ANGLES) * double(i);
		double dt = 1.0 / engineSpeed * (TOTAL_RADIANS / double(ANGLES));
		double volume = getVolume(angle, compressionVolume, crankRadius, rodLength, bore);

		state = compressAdiabatically(state, volume);

		if(angle > 1.0 * PI && !addedHeat) {
			double m = state.calculateSubstanceAmount() * 33.32;

			state = addHeat(state, m * 0.06 * (1000000.0 / 1000.0) * 46.7);
			addedHeat = true;
		}

		const double conductivity = 53.3;
		const double area = volume / (PI * std::pow(bore/2.0, 2.0)) * PI * bore;
		state = addHeat(state, -conductivity * (state.temperature - (273.0 + 200.0)) * area * dt);

		graph.push_back(std::make_pair(state.volume, state.pressure));
	}

	// TODO: model burning
	// TODO: model intake and exhaust


	return graph;
}

Joules Process::getWork(RadiansPerSecond engineSpeed)
{
	PressureVolumeGraph graph = getPvGraph(engineSpeed);

	return getAreaInPolygon(graph);
}

Process::Process(State beginningState, double compressionVolume, double crankRadius, double rodLength, double bore):
	beginningState(beginningState),
	gasConstant(R),
	compressionVolume(compressionVolume),
        crankRadius(crankRadius),
        rodLength(rodLength),
        bore(bore)
{

}

double Process::State::calculateSubstanceAmount() const
{
	double pv = pressure * volume;
	double rt = R * temperature;

	return pv / rt;
}

Process::State Process::addHeat(const Process::State& state, Joules heatAddition)
{
	State newState;

	double n = state.calculateSubstanceAmount();
	double m = n * 33.32;
	double Cv_gas = 2.22;
	double Cv_air = 0.718;
	double Cv_r = 0.06 * Cv_gas + 0.94 * Cv_air;
	double dT = heatAddition / (Cv_r * m);

	newState.volume = state.volume;
	newState.temperature = state.temperature + dT;
	newState.pressure = (R * n * newState.temperature) / newState.volume;

	return newState;
}

Process::State Process::compressAdiabatically(const Process::State &state, double newVolume)
{
	const double adiabaticIndexForDiatomicGases = 7.0 / 5.0;

	double x = state.pressure * std::pow(state.volume, adiabaticIndexForDiatomicGases);

	double newPressure = x / std::pow(newVolume, adiabaticIndexForDiatomicGases);

	State newState;

	newState.volume = newVolume;
	newState.pressure = newPressure;
	newState.temperature = (newVolume * newPressure) / (state.calculateSubstanceAmount() * R);

	return newState;
}

}
