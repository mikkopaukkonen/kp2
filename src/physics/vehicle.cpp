#include "vehicle.hpp"

#include <cmath>
#include <iostream>

namespace Physics
{

void Vehicle::advanceSimulation()
{
	const double PI = 3.14159265;
	const double RADS_TO_RPM = 60.0f / (2.0f * PI);
	const Seconds dt = 0.01d;

	state.time += dt;

	if(state.engineSpeed * RADS_TO_RPM > 1000)
		starter.setRunning(false);

	NewtonMeters engineTorque = 0.0d;

	engineTorque += engine.getTorque(state.engineSpeed);

	state.engineTorque = engineTorque;
	state.enginePower = state.engineTorque * state.engineSpeed;
	state.pvGraph = engine.graph;

	engineTorque += flywheel.getTorque();
	engineTorque += starter.getTorque(state.engineSpeed);

	double engineInertia = 0.0d;

	engineInertia += starter.getInertia();
	engineInertia += flywheel.getInertia();

	state.engineSpeed += engineTorque / engineInertia * dt;
}

const Vehicle::State& Vehicle::getState()
{
	return state;
}

Vehicle::Vehicle(const Game::Vehicle& vehicle):
	vehicle(vehicle),
	starter(),
	flywheel(),
	engine(findNumberOfCylinders(), findCylinderGeometry())
{
	starter.setRunning(true);

	state.enginePower = 0.0;
	state.engineSpeed = 0.0;
	state.engineTorque = 0.0;
	state.time = 0.0;
}

int Vehicle::findNumberOfCylinders()
{
	return getCylinderBlock().getCylinderCount();
}

Engine::CylinderGeometry Vehicle::findCylinderGeometry()
{
	Engine::CylinderGeometry geometry;

	geometry.bore = getCylinderBlock().getBore() / 1000.0;
	geometry.stroke = getCrankshaft().getStroke() / 1000.0;
	geometry.rodLength = getCrankshaft().getRod() / 1000.0;

	const double PI = 3.14159265;

	geometry.compressionVolume = PI * std::pow(geometry.bore / 2.0, 2.0) * getCylinderHead().getChamberHeight() / 1000.0;

	return geometry;
}

template <class T, class V>
T& getPartOfType(const V& parts) {
	auto it = std::find_if(parts.begin(), parts.end(), [&](Game::Part& ptr) {
		const T* t = dynamic_cast<T*>(&ptr);

		return t != nullptr;
	});

	if (it == parts.end()) {
		throw Vehicle::InvalidVehicleException();
	}

	return dynamic_cast<T&>(*it);
}

Game::Chassis& Vehicle::getChassis()
{
	return getPartOfType<Game::Chassis>(vehicle.getAttachedParts());
}

Game::CylinderBlock& Vehicle::getCylinderBlock()
{
	return getPartOfType<Game::CylinderBlock>(vehicle.getAttachedParts());
}

Game::Crankshaft& Vehicle::getCrankshaft()
{
	return getPartOfType<Game::Crankshaft>(vehicle.getAttachedParts());
}

Game::CylinderHead& Vehicle::getCylinderHead()
{
	return getPartOfType<Game::CylinderHead>(vehicle.getAttachedParts());
}

}
