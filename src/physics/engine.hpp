#ifndef PHYSICS_ENGINE_HPP
#define PHYSICS_ENGINE_HPP

#include "units.hpp"
#include "process.hpp"

namespace Physics
{

class Engine
{
	public:
		class CylinderGeometry
		{
			public:
				Meters bore;
				Meters stroke;
				Meters rodLength;
				CubicMeters compressionVolume;				
		};

		double getTorque(RadiansPerSecond engineSpeed);

		Engine(int cylinderCount, CylinderGeometry cylinderGeometry);

		PressureVolumeGraph graph;

	private:
		int cylinderCount;
		CylinderGeometry cylinderGeometry;
};

}

#endif
