#include "engine.hpp"

#include <cmath>
#include <iostream>

#include "process.hpp"
#include "math_tools.hpp"

namespace Physics
{

double Engine::getTorque(RadiansPerSecond engineSpeed)
{
	if(engineSpeed == 0.0) {
		return 0.0;
	}

	const double PI = 3.14159265;

	SquareMeters cylinderArea = PI * std::pow(cylinderGeometry.bore / 2.0, 2.0);

	CubicMeters displacementVolume = cylinderArea * cylinderGeometry.stroke;
	CubicMeters compressionVolume = cylinderGeometry.compressionVolume;

	Pascals atmospherePressure = 101325.0;
	Kelvins atmosphereTemperature = 273.15 + 20.0;

	Process::State state;

	double meanPistonSpeed = 2 * cylinderGeometry.stroke * (engineSpeed * RADS_TO_RPM) / 60;
	double resistanceCoeff = 0.8;

	double minimumArea = (8.8 / 1000.0) * PI * (13.95 / 1000.0);

	double deltaP = (1.2041) * pow(meanPistonSpeed, 2.0) * (resistanceCoeff * pow(cylinderArea / minimumArea, 2.0));
	state.pressure = atmospherePressure - deltaP - 70000.0;
	state.temperature = atmosphereTemperature;
	state.volume = (compressionVolume + displacementVolume);

	Process process(state, compressionVolume, cylinderGeometry.stroke / 2.0, cylinderGeometry.rodLength, cylinderGeometry.bore);

	double work = process.getWork(engineSpeed);
	graph = process.getPvGraph(engineSpeed);

	double meanPressure = work / displacementVolume;

	double totalDisplacement = displacementVolume * double(cylinderCount);

	double torque = (totalDisplacement * meanPressure) / (PI * 2.0 * 2.0);

//	std::cout << "displacement: " << displacementVolume * 1000.0 << " liters" << std::endl;
//	std::cout << "cylinders: " << cylinderCount << std::endl;
//	std::cout << "compressionRatio: " << (displacementVolume + compressionVolume) / compressionVolume << ":1" << std::endl;
//	std::cout << "displacementVolume: " << displacementVolume << " m^3" << std::endl;
//	std::cout << "manifoldPressure: " << manifoldPressure << " Pa" << std::endl;
//	std::cout << "volumetricEfficiency: " << volumetricEfficiency * 100.0 << " %" << std::endl;
//	std::cout << "sweptVolume: " << displacementVolume << " m^3" << std::endl;
//	std::cout << "work: " << work << " J" << std::endl;
//	std::cout << "meanPressure: " << meanPressure << " kPa" << std::endl;
//	std::cout << "torque: " << torque << " Nm" << std::endl;

	return torque;
}

Engine::Engine(int cylinderCount, CylinderGeometry cylinderGeometry):
	cylinderCount(cylinderCount),
	cylinderGeometry(cylinderGeometry)
{
}

}
