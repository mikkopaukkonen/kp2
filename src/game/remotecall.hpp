#ifndef GAME_REMOTECALL_HPP
#define GAME_REMOTECALL_HPP

#include <iostream>

#include "serializable.hpp"
#include "serializer.hpp"
#include "deserializer.hpp"
#include "player.hpp"
#include "session.hpp"

namespace Game
{

class RemoteCall: public Serializable {
	public:
		virtual void callOnLocalObject() const = 0;

};

template<class T>
class ObjectRemoteCall: public RemoteCall
{
	public:
		virtual Json::Value serialize(Serializer& serializer) const {
			Json::Value serialized;

			serialized["this"] = serializer.serializeAsReference(object);

			return serialized;
		}

		ObjectRemoteCall(const Json::Value& serialized, Deserializer& deserializer):
			object(deserializer.deserializeExpectingPointer<T>(serialized["this"]))
		{

		}

		ObjectRemoteCall(T* object):
			object(object)
		{

		}

	protected:		
		T* object;

};

class PlayerSetNameRemoteCall: public ObjectRemoteCall<Player> {
	public:
		Json::Value serialize(Serializer &serializer) const override;

		void callOnLocalObject() const override;

		PlayerSetNameRemoteCall(const Json::Value& serialized, Deserializer& deserializer);
		PlayerSetNameRemoteCall(Player* player, std::string name);

	protected:
		std::string name;
};

class PlayerBuyVehicleRemoteCall: public ObjectRemoteCall<Player> {
	public:
		Json::Value serialize(Serializer &serializer) const override;

		void callOnLocalObject() const override;

		PlayerBuyVehicleRemoteCall(const Json::Value& serialized, Deserializer& deserializer);

		PlayerBuyVehicleRemoteCall(Player* player, const State* state, const Vehicle* vehicle);

	protected:
		const State* state;
		const Vehicle* vehicle;
};

class PlayerBuyPartRemoteCall: public ObjectRemoteCall<Player> {
	public:
		Json::Value serialize(Serializer &serializer) const override;

		void callOnLocalObject() const override;

		PlayerBuyPartRemoteCall(const Json::Value& serialized, Deserializer& deserializer);

		PlayerBuyPartRemoteCall(Player* player, const State* state, const Part* part);

	protected:
		const State* state;
		const Part* part;
};


class PlayerSetActiveVehicleRemoteCall: public ObjectRemoteCall<Player> {
	public:
		Json::Value serialize(Serializer &serializer) const override;

		void callOnLocalObject() const override;

		PlayerSetActiveVehicleRemoteCall(const Json::Value& serialized, Deserializer& deserializer);

		PlayerSetActiveVehicleRemoteCall(Player* player, Vehicle* vehicle);

	protected:
		Vehicle* vehicle;
};


class SessionClaimPlayerRemoteCall: public ObjectRemoteCall<Session> {
	public:
		Json::Value serialize(Serializer &serializer) const override;

		void callOnLocalObject() const override;

		SessionClaimPlayerRemoteCall(const Json::Value& serialized, Deserializer& deserializer);

		SessionClaimPlayerRemoteCall(Session* session, std::string identifier);

	protected:
		std::string identifier;
};

class SessionSetPlayerRemoteCall: public ObjectRemoteCall<Session> {
	public:
		Json::Value serialize(Serializer &serializer) const override;

		void callOnLocalObject() const override;

		SessionSetPlayerRemoteCall(const Json::Value& serialized, Deserializer& deserializer);

		SessionSetPlayerRemoteCall(Session* session, Player* player);

	protected:
		Player* player;
};


class StateAddPlayerRemoteCall: public ObjectRemoteCall<State> {
	public:
		Json::Value serialize(Serializer &serializer) const override;

		void callOnLocalObject() const override;

		StateAddPlayerRemoteCall(const Json::Value& serialized, Deserializer& deserializer);

		StateAddPlayerRemoteCall(State* state, Player* player);

	protected:
		// HACK: this should not be mutable, deserializer lifetimes should be connection scoped so new instance can be created on demand?
		mutable std::unique_ptr<Player> ownedPlayer;
		Player* player;

};


class Proxied: public RemoteCall {
	public:
		void callOnLocalObject() const override {
			std::cout << __PRETTY_FUNCTION__ << ":" << i << std::endl;

			wrapped->callOnLocalObject();
		}

		Proxied(std::unique_ptr<RemoteCall> wrapped, int i):
		        wrapped(std::move(wrapped)),
		        i(i)
		{

		}

	private:
		std::unique_ptr<RemoteCall> wrapped;
		int i;

};


/*
		PlayerSetNameRemoteCall(Player* object, std::string name):
			RemoteCall(object),
			name(name)
		{
			Game::Object* object = objectIdMapper.getObject(call["id"].asString());


			if(object)
			{
				if(Game::Player* player = dynamic_cast<Game::Player*>(object))
				{
					if(player != &(this->player))
					{
						std::cout << "Connection invoked method on non-owned player." << std::endl;
					}

					std::string method = call["method"].asString();
					Json::Value& arguments = call["arguments"];

					if(method == "setName")
					{
						std::string name = arguments["name"].asString();

						player->setName(name);
					}
					else if(method == "setActiveVehicle")
					{
						Game::Vehicle* vehicle = objectIdMapper.getTypedObject<Game::Vehicle*>(arguments["vehicle"].asString());

						player->setActiveVehicle(vehicle);
					}
					else if(method == "buyPart")
					{
						Game::Part* part = objectIdMapper.getTypedObject<Game::Part*>(arguments["part"].asString());

						player->buyPart(part);
					}
					else if(method == "buyVehicle")
					{
						Game::Vehicle* vehicle = objectIdMapper.getTypedObject<Game::Vehicle*>(arguments["vehicleInShop"].asString());

						player->buyVehicle(vehicle);
					}
					else
					{
						std::cout << "Unimplemented method of class Player" << method << std::endl;
					}
				}
		};


}
*/

}

#endif // REMOTECALLARGUMENTS_HPP
