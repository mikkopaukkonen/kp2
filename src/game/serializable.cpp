#include "serializable.hpp"

#include "object.hpp"

namespace Game
{

void Serializable::save(Json::Value&) const
{

}

Json::Value Serializable::serialize(Serializer&) const
{
	Json::Value value;

	save(value);

	return value;
}

Serializable::Serializable(const Json::Value&)
{

}

Serializable::Serializable(const Json::Value& value, Deserializer& deserializer):
        Serializable(value)
{
	(void)deserializer;
}

}
