#ifndef GAME_SERIALIZABLE_HPP
#define GAME_SERIALIZABLE_HPP

#include <json/json.h>

namespace Game
{

class Deserializer;
class Serializer;

class Serializable
{
	public:
		virtual void save(Json::Value&) const;
		virtual Json::Value serialize(Serializer&) const;

		Serializable() = default;
		Serializable(const Serializable&) = default;
		Serializable(const Json::Value&);
		Serializable(const Json::Value&, Deserializer&);
		virtual ~Serializable() = default;
};

};

#endif
