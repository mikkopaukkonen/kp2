#ifndef GAME_OBJECTIDMAPPER_HPP
#define GAME_OBJECTIDMAPPER_HPP

#include "serializable.hpp"

#include <map>

namespace Game {

class ObjectIdMapper
{
	public:
		class UnknownObjectError {

		};

		class UnresolvedReferenceError {

		};

		Serializable* getObject(const std::string& id) const;
		std::string getOrCreateId(const Serializable* object);

		void setObjectId(Serializable* object, std::string id);
		Serializable* getObject(const std::string& id);

		ObjectIdMapper(const std::string& generatedIdPrefix = "", const Serializable* root = nullptr);

		std::string getAnyId(const Serializable* object) const;

		void setRoot(const Serializable* value);

	private:
		int nextId;
		std::string generatedIdPrefix;

		const Serializable* root;

		std::map<const Serializable*, std::string> idsByObject;
		std::map<std::string, Serializable*> objectsById;
};

}

#endif // OBJECTIDMAPPER_HPP
