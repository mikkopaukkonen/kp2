#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP

#include <string>
#include <memory>

#include "object.hpp"
#include "container.hpp"
#include "vehicle.hpp"
#include "part.hpp"
#include "player.hpp"
#include <stdint.h>

namespace Game
{

class State : public Object
{
	public:
		const Container<Player>& getPlayers() const;
		virtual Player* addPlayer(std::unique_ptr<Player> player);

		const Container<Vehicle>& getShopVehicles() const;
		Vehicle* addShopVehicle(std::unique_ptr<Vehicle> vehicle);
		std::unique_ptr<Vehicle> cloneShopVehicle(const Vehicle* vehicle) const;

		const Container<Part>& getShopParts() const;
		std::unique_ptr<Part> cloneShopPart(const Part* part) const;

		std::string getIdentifier() const;
		void setIdentifier(const std::string& value);

		Json::Value serialize(Serializer& serializer) const;

		State();
		State(const Json::Value&, Deserializer&);

	private:
		Container<Player> players;
		Container<Vehicle> vehicles;
		Container<Part> parts;

		std::string identifier;
};

};

#endif

