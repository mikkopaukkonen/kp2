#ifndef GAME_PLAYER_HPP
#define GAME_PLAYER_HPP

#include <string>
#include <memory>
#include <set>
#include <optional>

#include "part.hpp"
#include "vehicle.hpp"
#include "object.hpp"
#include "container.hpp"
#include "exception.hpp"

namespace Game
{

class State;

class Player : public Object
{
	public:
		class InsufficientFundsException : public Game::Exception
		{

		};

		class NoSuchVehicleException : public Game::Exception
		{

		};

		class NoSuchPartException : public Game::Exception
		{

		};

		virtual void setName(const std::string& name);
		virtual void setActiveVehicle(Vehicle* vehicle);

		virtual void addMoney(int money);

		virtual void buyPart(const State* state, const Part* part);
		virtual void buyVehicle(const State* state, const Vehicle* vehicle);

		const std::string& getName() const;

		int getMoney() const;

		const Container<Vehicle>& getVehicles() const;
		const Container<Part>& getParts() const;

		Vehicle* getActiveVehicle() const;

		bool isIdentifiedBy(std::string identifier);
		void setIdentifier(const std::string& value);

		Player();
		Player(std::string identifier);
		Player(const Json::Value& value, Deserializer& deserializer);

		virtual Json::Value serialize(Serializer& serializer) const;

	private:
		std::string name;
		int money;

		Container<Part> parts;
		Container<Vehicle> vehicles;

		Vehicle* activeVehicle;

		std::optional<std::string> identifier;
};

};

#endif
