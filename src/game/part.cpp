#include "part.hpp"

namespace Game
{

int Part::getPrice() const
{
	return price;
}

float Part::getMass() const
{
	return mass;
}

Part::Part(int price, float mass):
	price(price),
	mass(mass)
{

}

Part::Part(const Json::Value& value):
	PartContainer(value)
{
	price = value["price"].asInt();
	mass = value["mass"].asDouble();
}

void Part::save(Json::Value& value) const
{
	PartContainer::save(value);

	value["price"] = price;
	value["mass"] = mass;
}

}
