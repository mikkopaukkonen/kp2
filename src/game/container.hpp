#ifndef GAME_CONTAINER_HPP
#define GAME_CONTAINER_HPP

#include <vector>
#include <algorithm>
#include <memory>
#include <functional>
#include <boost/iterator/indirect_iterator.hpp>

#include "json/json.h"
#include "object.hpp"
#include "serializable.hpp"
#include "deserializer.hpp"
#include "serializer.hpp"

namespace Game
{

template<class T>
class Container: public Serializable
{
	public:
		typedef std::vector<std::unique_ptr<T>> ContainerType;

		class Listener
		{
			public:
				//static std::function<void(Container<T>*, int)> NOOP = [](Container<T>*, int) {};

				void onChange(Container<T>* container, int index) {
					onChangeHandler(container, index);
				}

				void onAdd(Container<T>* container, int index) {
					onAddHandler(container, index);
				}

				void onRemove(Container<T>* container, int index) {
					onRemoveHandler(container, index);
				}

				Listener(std::function<void(Container<T>*, int)> onChangeHandler, std::function<void(Container<T>*, int)> onAddHandler,
				         std::function<void(Container<T>*, int)> onRemoveHandler):
				        onChangeHandler(onChangeHandler),
				        onAddHandler(onAddHandler),
				        onRemoveHandler(onRemoveHandler)
				{}

			private:
				std::function<void(Container<T>*, int)> onChangeHandler;
				std::function<void(Container<T>*, int)> onAddHandler;
				std::function<void(Container<T>*, int)> onRemoveHandler;

		};

		boost::indirect_iterator<typename ContainerType::iterator> begin()
		{
			return items.begin();
		};

		boost::indirect_iterator<typename ContainerType::const_iterator> begin() const
		{
			return items.begin();
		};

		boost::indirect_iterator<typename ContainerType::iterator> end()
		{
			return items.end();
		};

		boost::indirect_iterator<typename ContainerType::const_iterator> end() const
		{
			return items.end();
		};

		int getIndexOf(const T* item) const
		{
			auto it = std::find_if(items.begin(), items.end(), [&](const std::unique_ptr<T>& p) { return p.get() == item;});

			if(it == items.end())
				return -1;

			return std::distance(items.begin(), it);
		};

		T* getByIndex(int index) const
		{
			if(index < 0)
				return nullptr;

			auto it = items.begin();

			std::advance(it, index);

			if(it == items.end())
				return nullptr;

			return it->get();
		};

		bool contains(const T* item) const
		{
			auto it = std::find_if(items.begin(), items.end(), [&](const std::unique_ptr<T>& p) { return p.get() == item;});

			if(it == items.end())
				return false;

			return true;
		};

		int size() const
		{
			return items.size();
		}

		virtual void add(std::unique_ptr<T>&& item)
		{
			using namespace std::placeholders;

			T* addedItem = item.get();

			items.push_back(std::move(item));

			added(items.size() - 1);

			addedItem->addListener(Object::Listener(std::bind(&Container::onChange, this, _1)));
		};

		virtual std::unique_ptr<T> remove(T* item)
		{
			auto it = std::find_if(items.begin(), items.end(), [&](std::unique_ptr<T>& p) { return p.get() == item;});

			if(it == items.end())
				throw std::out_of_range("No such part");

			int index = std::distance(items.begin(), it);

			connections.erase(item);

			auto removedPart = std::move(*it);

			items.erase(it);

			removed(index);

			return removedPart;
		};

		virtual Json::Value serialize(Serializer& serializer) const
		{
			Json::Value value;
			value["items"].resize(0);

			for(auto& item : items)
			{
				value["items"].append(serializer.serialize(*item.get()));
			}

			return value;
		};

		std::shared_ptr<Listener> addListener(Listener listener) const
		{
			auto sharedListener = std::make_shared<Listener>(listener);

			listeners.push_back(std::weak_ptr<Listener>(sharedListener));

			return sharedListener;
		};

		Container(const Json::Value& value, Deserializer& deserializer)
		{
			for(auto item : value["items"])
			{
				add(deserializer.deserializeExpectingType<T>(item));
			}
		};

		void onChange(Object* object)
		{
			auto it = std::find_if(items.begin(), items.end(), [&](std::unique_ptr<T>& p) { return p.get() == object;});

			if(it == items.end())
				return;

			int index = std::distance(items.begin(), it);

			changed(index);
		}

		Container() = default;

	protected:
		Container(const Container& b)
		{
			*this = b;
		};

	private:
		void added(int index)
		{
			for(auto listener : listeners) {
				if(auto sharedListener = listener.lock()) {
					sharedListener->onAdd(this, index);
				}
			}
		}

		void removed(int index)
		{
			for(auto listener : listeners) {
				if(auto sharedListener = listener.lock()) {
					sharedListener->onRemove(this, index);
				}
			}
		}

		void changed(int index)
		{
			for(auto listener : listeners) {
				if(auto sharedListener = listener.lock()) {
					sharedListener->onChange(this, index);
				}
			}
		}

		ContainerType items;

		mutable std::map<Object*, std::shared_ptr<Listener>> connections;
		mutable std::vector<std::weak_ptr<Listener>> listeners;
};

};

#endif
