#include "remotecall.hpp"

#include "state.hpp"

#include <cassert>

namespace Game
{

Json::Value PlayerSetNameRemoteCall::serialize(Serializer& serializer) const {
	Json::Value serialized = ObjectRemoteCall::serialize(serializer);

	serialized["type"] = "PlayerSetNameRemoteCall";
	serialized["name"] = name;

	return serialized;
}

void PlayerSetNameRemoteCall::callOnLocalObject() const {
	object->setName(name);
}

PlayerSetNameRemoteCall::PlayerSetNameRemoteCall(const Json::Value& serialized, Deserializer& deserializer):
        ObjectRemoteCall(serialized, deserializer),
        name(serialized["name"].asString())
{

}

PlayerSetNameRemoteCall::PlayerSetNameRemoteCall(Player* player, std::string name):
        ObjectRemoteCall(player),
        name(name)
{

}

Json::Value PlayerBuyVehicleRemoteCall::serialize(Serializer& serializer) const {
	Json::Value serialized = ObjectRemoteCall::serialize(serializer);

	serialized["type"] = "PlayerBuyVehicleRemoteCall";
	serialized["state"] = serializer.serializeAsReference(state);
	serialized["vehicle"] = serializer.serializeAsReference(vehicle);

	return serialized;
}

void PlayerBuyVehicleRemoteCall::callOnLocalObject() const {
	object->buyVehicle(state, vehicle);
}

PlayerBuyVehicleRemoteCall::PlayerBuyVehicleRemoteCall(const Json::Value& serialized, Deserializer& deserializer):
        ObjectRemoteCall(serialized, deserializer),
        state(deserializer.deserializeExpectingPointer<State>(serialized["state"])),
        vehicle(deserializer.deserializeExpectingPointer<Vehicle>(serialized["vehicle"]))
{

}

PlayerBuyVehicleRemoteCall::PlayerBuyVehicleRemoteCall(Player* player, const State* state,  const Vehicle* vehicle):
        ObjectRemoteCall(player),
        state(state),
        vehicle(vehicle)
{

}

Json::Value PlayerSetActiveVehicleRemoteCall::serialize(Serializer& serializer) const
{
	Json::Value serialized = ObjectRemoteCall::serialize(serializer);

	serialized["type"] = "PlayerSetActiveVehicleRemoteCall";
	serialized["vehicle"] = serializer.serializeAsReference(vehicle);

	return serialized;
}

void PlayerSetActiveVehicleRemoteCall::callOnLocalObject() const
{
	object->setActiveVehicle(vehicle);
}

PlayerSetActiveVehicleRemoteCall::PlayerSetActiveVehicleRemoteCall(const Json::Value& serialized, Deserializer& deserializer):
        ObjectRemoteCall(serialized, deserializer),
        vehicle(deserializer.deserializeExpectingPointer<Vehicle>(serialized["vehicle"]))
{

}

PlayerSetActiveVehicleRemoteCall::PlayerSetActiveVehicleRemoteCall(Player* player, Vehicle* vehicle):
        ObjectRemoteCall(player),
        vehicle(vehicle)
{

}

Json::Value PlayerBuyPartRemoteCall::serialize(Serializer& serializer) const
{
	Json::Value serialized = ObjectRemoteCall::serialize(serializer);

	serialized["type"] = "PlayerBuyPartRemoteCall";
	serialized["state"] = serializer.serializeAsReference(state);
	serialized["part"] = serializer.serializeAsReference(part);

	return serialized;
}

void PlayerBuyPartRemoteCall::callOnLocalObject() const
{
	object->buyPart(state, part);
}

PlayerBuyPartRemoteCall::PlayerBuyPartRemoteCall(const Json::Value& serialized, Deserializer& deserializer):
        ObjectRemoteCall(serialized, deserializer),
        state(deserializer.deserializeExpectingPointer<State>(serialized["state"])),
        part(deserializer.deserializeExpectingPointer<Part>(serialized["part"]))
{

}

PlayerBuyPartRemoteCall::PlayerBuyPartRemoteCall(Player* player, const State* state, const Part* part):
        ObjectRemoteCall(player),
        state(state),
        part(part)
{

}

Json::Value SessionClaimPlayerRemoteCall::serialize(Serializer& serializer) const
{
	Json::Value serialized = ObjectRemoteCall::serialize(serializer);

	serialized["type"] = "SessionClaimPlayerRemoteCall";
	serialized["identifier"] = identifier;

	return serialized;
}

void SessionClaimPlayerRemoteCall::callOnLocalObject() const
{
	object->claimPlayer(identifier);
}

SessionClaimPlayerRemoteCall::SessionClaimPlayerRemoteCall(const Json::Value& serialized, Deserializer& deserializer):
        ObjectRemoteCall(serialized, deserializer),
        identifier(serialized["identifier"].asString())
{

}

SessionClaimPlayerRemoteCall::SessionClaimPlayerRemoteCall(Session* session, std::string identifier):
        ObjectRemoteCall(session),
        identifier(identifier)
{

}

Json::Value SessionSetPlayerRemoteCall::serialize(Serializer& serializer) const
{
	Json::Value serialized = ObjectRemoteCall::serialize(serializer);

	serialized["type"] = "SessionSetPlayerRemoteCall";
	serialized["player"] = serializer.serializeAsReference(player);

	return serialized;
}

void SessionSetPlayerRemoteCall::callOnLocalObject() const
{
	object->setPlayer(player);
}

SessionSetPlayerRemoteCall::SessionSetPlayerRemoteCall(const Json::Value& serialized, Deserializer& deserializer):
        ObjectRemoteCall(serialized, deserializer),
        player(deserializer.deserializeExpectingPointer<Player>(serialized["player"]))
{

}

SessionSetPlayerRemoteCall::SessionSetPlayerRemoteCall(Session* session, Player* player):
        ObjectRemoteCall(session),
        player(player)
{

}

Json::Value StateAddPlayerRemoteCall::serialize(Serializer& serializer) const
{
	Json::Value serialized = ObjectRemoteCall::serialize(serializer);

	serialized["type"] = "StateAddPlayerRemoteCall";
	serialized["player"] = serializer.serialize(*player);

	return serialized;
}

void StateAddPlayerRemoteCall::callOnLocalObject() const
{
	assert(ownedPlayer);

	object->addPlayer(std::move(ownedPlayer));
}

StateAddPlayerRemoteCall::StateAddPlayerRemoteCall(const Json::Value& serialized, Deserializer& deserializer):
        ObjectRemoteCall(serialized, deserializer),
        ownedPlayer(deserializer.deserializeExpectingType<Player>(serialized["player"])),
	player(ownedPlayer.get())
{

}

StateAddPlayerRemoteCall::StateAddPlayerRemoteCall(State* state, Player* player):
        ObjectRemoteCall(state),
        player(player)
{

}


}
