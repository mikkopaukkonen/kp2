#include "partcontainer.hpp"

#include "part.hpp"

namespace Game
{

Part* PartContainer::getPart() const
{
	return nullptr;
}

PartContainer::Slots PartContainer::getSlots() const
{
	return registeredSlots;
}

Slot& PartContainer::getSlotByName(const std::string& name) const
{
	return registeredSlots.at(name);
}

PartContainer::PartContainer(const Json::Value& value):
	Object(value)
{

}

void PartContainer::registerSlot(Slot& slot)
{
	registeredSlots.insert(std::make_pair(slot.getName(), std::reference_wrapper(slot)));
}

}
