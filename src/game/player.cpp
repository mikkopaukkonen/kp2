#include "player.hpp"

#include <algorithm>

#include "deserializer.hpp"
#include "serializer.hpp"

#include "state.hpp"

namespace Game
{

void Player::setName(const std::string& name)
{
	this->name = name;

	changed();
}

void Player::setActiveVehicle(Vehicle* vehicle)
{
	if(vehicles.contains(vehicle))
		activeVehicle = vehicle;
	else
		activeVehicle = 0;

	changed();
}

void Player::addMoney(int money)
{
	this->money += money;

	changed();
}

void Player::buyPart(const State* state, const Part* part)
{
	if(money < part->getPrice())
		throw InsufficientFundsException();

	money -= part->getPrice();

	parts.add(state->cloneShopPart(part));

	changed();
}

void Player::buyVehicle(const State* state, const Vehicle* vehicle)
{
	if(money < vehicle->getPrice())
		throw InsufficientFundsException();

	money -= vehicle->getPrice();

	vehicles.add(state->cloneShopVehicle(vehicle));

	changed();
}

const std::string& Player::getName() const
{
	return name;
}

int Player::getMoney() const
{
	return money;
}

const Container<Vehicle>& Player::getVehicles() const
{
	return vehicles;
}

const Container<Part>& Player::getParts() const
{
	return parts;
}

Vehicle* Player::getActiveVehicle() const
{
	return activeVehicle;
}

bool Player::isIdentifiedBy(std::string identifier)
{
	if(!this->identifier.has_value()) {
		return false;
	}

	return identifier == this->identifier.value();
}

Player::Player():
        money(0),
        activeVehicle(nullptr)
{

}

Player::Player(std::string identifier):
	money(0),
        activeVehicle(nullptr),
        identifier(identifier)
{

}

Player::Player(const Json::Value& value, Deserializer& deserializer):
	Object(value),
        name(value["name"].asString()),
        money(value["money"].asUInt()),
	parts(value["parts"], deserializer),
	vehicles(value["vehicles"], deserializer),
        activeVehicle(deserializer.deserializeExpectingPointer<Vehicle>(value["activeVehicle"])),
        identifier(value["identifier"].isString() ? std::make_optional(value["identifier"].asString()) : std::nullopt)
{

}

Json::Value Player::serialize(Serializer& serializer) const
{
	Json::Value value = Object::serialize(serializer);

	value["type"] = "Player";
	value["name"] = name;
	value["money"] = money;

	value["vehicles"] = serializer.serialize(vehicles);
	value["parts"] = serializer.serialize(parts);

	value["activeVehicle"] = serializer.serializeAsReference(activeVehicle);

	if(identifier.has_value())
		value["identifier"] = identifier.value();
	else
		value["identifier"] = Json::Value();

	return value;
}

void Player::setIdentifier(const std::string& value)
{
	identifier = value;
}

};
