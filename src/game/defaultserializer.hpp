#ifndef GAME_DEFAULTSERIALIZER_HPP
#define GAME_DEFAULTSERIALIZER_HPP

#include "serializer.hpp"
#include "deserializer.hpp"
#include "objectidmapper.hpp"

namespace Game {

class DefaultSerializer: public Serializer
{
	public:
		Json::Value serialize(const Serializable& object);
		Json::Value serializeAsReference(const Serializable* object);

		DefaultSerializer(ObjectIdMapper& objectIdMapper);

	private:
		ObjectIdMapper& objectIdMapper;
};

class DefaultDeserializer: public Deserializer
{
	public:
		std::unique_ptr<Serializable> deserialize(const Json::Value& value);
		Serializable* deserializeAsReference(const Json::Value& v);

		DefaultDeserializer(ObjectIdMapper& objectIdMapper);

	protected:

		template<class T>
		T getTypedObject(const std::string& id)
		{
			return dynamic_cast<T>(objectIdMapper.getObject(id));
		}

		virtual std::unique_ptr<Serializable> instantiate(std::string type, const Json::Value& value);

	private:
		ObjectIdMapper& objectIdMapper;
};

class DefaultSerializerDeserializer:  public DefaultSerializer, public DefaultDeserializer
{
	public:
		DefaultSerializerDeserializer(ObjectIdMapper& objectIdMapper);
};

}

#endif // DEFAULTSERIALIZER_HPP
