#include "state.hpp"

#include "utils/directory.hpp"

#include "defaultserializer.hpp"

#include <stdexcept>
#include <iostream>
#include <sstream>
#include <memory>

namespace Game
{

Player* State::addPlayer(std::unique_ptr<Player> player)
{
	Player* ptr = player.get();

	players.add(std::move(player));

	return ptr;
}

const Container<Player>& State::getPlayers() const
{
	return players;
}

const Container<Vehicle>& State::getShopVehicles() const
{
	return vehicles;
}

Vehicle* State::addShopVehicle(std::unique_ptr<Vehicle> vehicle)
{
	Vehicle* ptr = vehicle.get();

	vehicles.add(std::move(vehicle));

	return ptr;
}

std::unique_ptr<Vehicle> State::cloneShopVehicle(const Vehicle* vehicle) const
{
	ObjectIdMapper objectIdMapper;
	DefaultSerializerDeserializer serializer(objectIdMapper);

	return serializer.deserializeExpectingType<Vehicle>(serializer.serialize(*vehicle));
}

const Container<Part>& State::getShopParts() const
{
	return parts;
}

std::unique_ptr<Part> State::cloneShopPart(const Part* part) const
{
	ObjectIdMapper objectIdMapper;
	DefaultSerializerDeserializer serializer(objectIdMapper);

	return serializer.deserializeExpectingType<Part>(serializer.serialize(*part));
}

Json::Value State::serialize(Serializer& serializer) const
{
	Json::Value serialized = Object::serialize(serializer);

	serialized["type"] = "State";

	serialized["players"] = serializer.serialize(players);
	serialized["vehicles"] = serializer.serialize(vehicles);
	serialized["parts"] = serializer.serialize(parts);
	serialized["identifier"] = identifier;

	return serialized;
}

State::State()
{

}

State::State(const Json::Value& value, Deserializer& deserializer):
	Object(value),
	players(value["players"], deserializer),
	vehicles(value["vehicles"], deserializer),
	parts(value["parts"], deserializer),
        identifier(value["identifier"].asString())
{

}

std::string State::getIdentifier() const
{
    return identifier;
}

void State::setIdentifier(const std::string& value)
{
	identifier = value;
}

};
