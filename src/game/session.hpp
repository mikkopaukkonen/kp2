#ifndef GAME_SESSION_HPP
#define GAME_SESSION_HPP

#include "object.hpp"
#include "player.hpp"

namespace Game {

class Session: public Object
{
	public:
		virtual void claimPlayer(std::string identifier) = 0;

		virtual Player* getPlayer() const;
		virtual void setPlayer(Player* value);

		Json::Value serialize(Serializer &) const;

		Session();
		Session(const Json::Value serialized, Deserializer& deserializer);

	private:
		Player* player;


};

}

#endif // SESSION_HPP
