#ifndef GAME_PARTCONTAINER_HPP
#define GAME_PARTCONTAINER_HPP

#include <string>
#include <map>
#include <memory>

#include "object.hpp"
#include "slot.hpp"

namespace Game
{

class Part;

class PartContainer : public Object
{
	public:
		typedef const std::map<std::string, std::reference_wrapper<Slot>>& Slots;

		Part* getPart() const;

		Slots getSlots() const;
		Slot& getSlotByName(const std::string& name) const;

		PartContainer() = default;
		PartContainer(const Json::Value&);

	protected:
		void registerSlot(Slot& slot);

	private:
		std::map<std::string, std::reference_wrapper<Slot>> registeredSlots;
};

};

#endif
