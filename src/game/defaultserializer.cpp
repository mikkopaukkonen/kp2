#include "defaultserializer.hpp"

#include <stdexcept>
#include <memory>
#include <cassert>
#include <sstream>
#include <cstring>
#include <iterator>

#include "game/chassis.hpp"
#include "game/crankshaft.hpp"
#include "game/cylinderblock.hpp"
#include "game/cylinderhead.hpp"
#include "game/vehicle.hpp"
#include "game/player.hpp"
#include "game/remotecall.hpp"
#include "game/state.hpp"
#include "game/connectionstate.hpp"
#include "game/session.hpp"

#include "serializer.hpp"

namespace Game {

Json::Value DefaultSerializer::serialize(const Serializable& object)
{
	Json::Value value = object.serialize(*this);

	if(value.isObject()) {
		value["__id"] = objectIdMapper.getOrCreateId(&object);
		value["__pointer"] = (uintptr_t)(&object);
	}

	return value;
}

Json::Value DefaultSerializer::serializeAsReference(const Serializable* object)
{
	Json::Value value;

	value["type"] = "__ref";

	if(object == nullptr) {
		value["id"] = Json::Value::null;
	} else {
		value["id"] = objectIdMapper.getAnyId(object);
	}

	return value;
}

DefaultSerializer::DefaultSerializer(ObjectIdMapper& objectIdMapper):
        objectIdMapper(objectIdMapper)
{

}

std::unique_ptr<Serializable> DefaultDeserializer::deserialize(const Json::Value& value)
{
	std::string type = value["type"].asString();

	std::unique_ptr<Serializable> ptr = instantiate(type, value);

	if(!ptr) {
		throw std::runtime_error("Invalid object type \"" + type + "\"");
	}

	if(value.isMember("__id")) {
		objectIdMapper.setObjectId(ptr.get(), value["__id"].asString());
	} else {
		objectIdMapper.setObjectId(ptr.get(), objectIdMapper.getOrCreateId(ptr.get()));
	}

	return ptr;
}

Serializable* DefaultDeserializer::deserializeAsReference(const Json::Value& v)
{
	if (v["type"] != "__ref") {
		throw std::bad_cast();
	}

	if (v["id"].isNull()) {
		return nullptr;
	}

	return objectIdMapper.getObject(v["id"].asString());
}

DefaultDeserializer::DefaultDeserializer(ObjectIdMapper& objectIdMapper):
        objectIdMapper(objectIdMapper)
{

}

std::unique_ptr<Serializable> DefaultDeserializer::instantiate(std::string type, const Json::Value& value)
{
	if(type == "Chassis")
		return std::make_unique<Game::Chassis>(value);
	else if(type == "Crankshaft")
		return std::make_unique<Game::Crankshaft>(value);
	else if(type == "CylinderBlock")
		return std::make_unique<Game::CylinderBlock>(value);
	else if(type == "CylinderHead")
		return std::make_unique<Game::CylinderHead>(value);
	else if(type == "Vehicle")
		return std::make_unique<Game::Vehicle>(value, *this);
	else if(type == "Player")
		return std::make_unique<Game::Player>(value, *this);
	else if(type == "State")
		return std::make_unique<Game::State>(value, *this);
	else if(type == "ConnectionState")
		return std::make_unique<Game::ConnectionState>(value, *this);
	else if(type == "PlayerBuyVehicleRemoteCall")
		return std::make_unique<Game::PlayerBuyVehicleRemoteCall>(value, *this);
	else if(type == "PlayerSetNameRemoteCall")
		return std::make_unique<Game::PlayerSetNameRemoteCall>(value, *this);
	else if(type == "PlayerSetActiveVehicleRemoteCall")
		return std::make_unique<Game::PlayerSetActiveVehicleRemoteCall>(value, *this);
	else if(type == "PlayerBuyPartRemoteCall")
		return std::make_unique<Game::PlayerBuyPartRemoteCall>(value, *this);
	else if(type == "SessionClaimPlayerRemoteCall")
		return std::make_unique<Game::SessionClaimPlayerRemoteCall>(value, *this);
	else if(type == "SessionSetPlayerRemoteCall")
		return std::make_unique<Game::SessionSetPlayerRemoteCall>(value, *this);
	else if(type == "StateAddPlayerRemoteCall")
		return std::make_unique<Game::StateAddPlayerRemoteCall>(value, *this);

	return nullptr;
}

DefaultSerializerDeserializer::DefaultSerializerDeserializer(ObjectIdMapper& objectIdMapper):
        DefaultSerializer(objectIdMapper),
        DefaultDeserializer(objectIdMapper)
{

}


}
