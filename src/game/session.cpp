#include "session.hpp"

namespace Game {

Session::Session():
        player(nullptr)
{

}

Session::Session(const Json::Value serialized, Deserializer& deserializer):
        player(deserializer.deserializeExpectingPointer<Player>(serialized["player"]))
{

}

Player* Session::getPlayer() const
{
	return player;
}

void Session::setPlayer(Player* value)
{
	player = value;

	changed();
}

Json::Value Session::serialize(Serializer& serializer) const
{
	Json::Value serialized = Object::serialize(serializer);

	serialized["type"] = "Session";
	serialized["player"] = serializer.serializeAsReference(player);

	return serialized;
}

}
