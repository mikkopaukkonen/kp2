#ifndef GAME_SERIALIZER_HPP
#define GAME_SERIALIZER_HPP

#include <memory>
#include <functional>

#include "serializable.hpp"
#include "objectidmapper.hpp"

namespace Game {

class Serializer
{
	public:
		virtual Json::Value serialize(const Serializable& object) = 0;
		virtual Json::Value serializeAsReference(const Serializable* object) = 0;

		virtual ~Serializer() = default;
};

}

#endif // SERIALIZER_HPP
