#ifndef GAME_VEHICLE_HPP
#define GAME_VEHICLE_HPP

#include "partcontainer.hpp"
#include "container.hpp"
#include "chassis.hpp"

#include <string>
#include <memory>

namespace Game
{

class Vehicle: public PartContainer
{
	public:
		virtual const std::string& getName() const;
		virtual const std::string& getInfo() const;
		float getMass() const;
		virtual int getYear() const;
		virtual int getPrice() const;
		const std::string& getImageName() const;

		const Container<Part>& getAttachedParts() const;
		Part* getAttachedPart(const Part& attachedTo, const Slot& slot) const;
		void attachPart(std::unique_ptr<Part>& part);
		std::vector<std::unique_ptr<Part>> detachPart(Part& part);

		void applyPropertiesOf(const Vehicle& vehicle);

		Vehicle(const std::string& name, int price, int year, const std::string& info, const std::string& imageName);
		Vehicle(const Json::Value&, Deserializer& deserializer);
		virtual void save(Json::Value& value) const;

		typedef std::pair<const PartContainer*, const std::string> PartAttachment;

	private:
		std::string name;
		int price;
		int year;
		std::string info;
		std::string imageName;

		Container<Part> parts;

		std::map<PartAttachment, Part*> attachments;

		TypedSlot<Chassis, Vehicle> chassisSlot;
};

};

#endif

