#include "chassis.hpp"

namespace Game
{

float Chassis::getDragCoefficient() const
{
	return dragCoefficient;
}

const Chassis::Dimensions& Chassis::getDimensions() const
{
	return dimensions;
}

const Chassis::EngineConstraints& Chassis::getEngineConstraints() const
{
	return engineConstraints;
}

void Chassis::applyPropertiesOf(const Chassis& chassis)
{
	dragCoefficient = chassis.getDragCoefficient();
	dimensions = chassis.getDimensions();
	engineConstraints = chassis.getEngineConstraints();
}

Chassis::Chassis(float mass, float dragCoefficient, const Dimensions& dimensions, const EngineConstraints& engineConstraints):
	Part(0, mass),
	dragCoefficient(dragCoefficient),
	dimensions(dimensions),
	engineConstraints(engineConstraints),
        cylinderBlockSlot("cylinderBlock", *this)
{
	registerSlot(cylinderBlockSlot);
}

Chassis::Chassis(const Json::Value& value):
	Part(value),
	dragCoefficient(value["dragCoefficient"].asDouble()),
        dimensions{value["dimensions"]["length"].asDouble(), value["dimensions"]["width"].asDouble(), value["dimensions"]["height"].asDouble(), value["dimensions"]["wheelbase"].asDouble(), value["dimensions"]["axleTrack"].asDouble()},
        engineConstraints{value["engineConstraints"]["maxEngineVolume"].asInt(), value["engineConstraints"]["maxEngineCylinderCount"].asInt()},
        cylinderBlockSlot("cylinderBlock", *this)
{	
	registerSlot(cylinderBlockSlot);
}

void Chassis::save(Json::Value& value) const
{
	Part::save(value);

	value["type"] = "Chassis";
	value["dragCoefficient"] = dragCoefficient;
	value["dimensions"]["length"] = dimensions.length;
	value["dimensions"]["width"] = dimensions.width;
	value["dimensions"]["height"] = dimensions.height;
	value["dimensions"]["wheelbase"] = dimensions.wheelbase;
	value["dimensions"]["axleTrack"] = dimensions.axleTrack;
	value["engineConstraints"]["maxEngineVolume"] = engineConstraints.maxVolume;
	value["engineConstraints"]["maxEngineCylinderCount"] = engineConstraints.maxCylinderCount;
}

}
