#ifndef GAME_SLOT_HPP
#define GAME_SLOT_HPP

#include <memory>

namespace Game {

class Part;

class Slot
{
	public:
		const std::string& getName() const;
		virtual bool canAttachPart(const Part& part) const;

		Slot(const std::string& name);

	private:
		const std::string name;
};

template <class T, class U>
class TypedSlot : public Slot
{
	public:
		virtual bool canAttachPart(const Part& part) const {
			if (const T* typedPart = dynamic_cast<const T*>(&part)) {
				const U& u = to;
				return canAttachPart(*typedPart, u);
			}

			return false;
		}

		TypedSlot(const std::string& name, const U& to):
			Slot(name),
		        to(to)
		{

		}

		virtual bool canAttachPart(const T& part, const U& to) const {
			(void)part;
			(void)to;

			return true;
		}

	private:
		const U& to;

};

}

#endif // SLOT_HPP
