#include "crankshaft.hpp"

namespace Game
{

double Crankshaft::getStroke() const
{
	return stroke;
}

double Crankshaft::getRod() const
{
	return rod;
}

int Crankshaft::getCylinderCount() const
{
	return cylinderCount;
}

void Crankshaft::applyPropertiesOf(const Crankshaft& crankshaft)
{
	cylinderCount = crankshaft.getCylinderCount();
	stroke = crankshaft.getStroke();
	rod = crankshaft.getRod();
}

Crankshaft::Crankshaft(double stroke, double rod):
	stroke(stroke),
	rod(rod)
{

}

Crankshaft::Crankshaft(const Json::Value& value):
	Part(value)
{
	cylinderCount = value["cylinderCount"].asUInt();
	stroke = value["stroke"].asDouble();
	rod = value["rod"].asDouble();
}

void Crankshaft::save(Json::Value& value) const
{
	Part::save(value);

	value["type"] = "Crankshaft";
	value["cylinderCount"] = cylinderCount;
	value["stroke"] = stroke;
	value["rod"] = rod;
}

}
