#include "objectidmapper.hpp"

#include <sstream>
#include <istream>
#include <iterator>
#include <iostream>
#include <cstring>

#include "defaultserializer.hpp"

namespace Game {

std::optional<Serializable*> getObjectByPath(const Json::Value& serializedState, const std::string& path);
std::optional<std::string> getObjectPath(const Json::Value& serializedState, const Serializable* object);
Json::Value serializeRoot(const Serializable* root);

void ObjectIdMapper::setObjectId(Serializable* object, std::string id)
{
	idsByObject[object] = id;
	objectsById[id] = object;
}

std::string ObjectIdMapper::getAnyId(const Serializable* object) const
{
	auto it = idsByObject.find(object);

	if (it != idsByObject.end()) {
		return it->second;
	}

	// TODO: is there a way to assign ids to objects on creation so absolute paths are not needed?
	auto path = getObjectPath(serializeRoot(root), object);

	if (!path.has_value())
	{
		throw UnknownObjectError();
	}

	std::string res = path.value();

	std::cout << "got mapping " << object << " -> '" << res << "'" << std::endl;

	return res;
}

void ObjectIdMapper::setRoot(const Serializable* value)
{
	root = value;
}


Serializable* ObjectIdMapper::getObject(const std::string& id)
{
	auto it = objectsById.find(id);

	if (it != objectsById.end()) {
		return it->second;
	}

	// TODO: is there a way to assign ids to objects on creation so absolute paths are not needed?
	auto res = getObjectByPath(serializeRoot(root), id);

	if (!res.has_value())
	{
		throw UnresolvedReferenceError();
	}

	Game::Serializable* object = res.value();

	std::cout << "got mapping " << id << " -> '" << object << "'" << std::endl;

	return object;
}

std::string ObjectIdMapper::getOrCreateId(const Serializable* object)
{
	auto it = idsByObject.find(object);

	if (it != idsByObject.end()) {
		return it->second;
	}

	std::stringstream ss;

	ss << nextId;

	std::string newId = generatedIdPrefix + ss.str();

	idsByObject[object] = newId;
	objectsById[newId] = const_cast<Serializable*>(object);

	nextId++;

	return newId;
}

ObjectIdMapper::ObjectIdMapper(const std::string& generatedIdPrefix, const Serializable* root):
        nextId(0),
        generatedIdPrefix(generatedIdPrefix),
        root(root)
{

}

std::optional<Serializable*> getObjectByPath(const Json::Value& serializedState, const std::string& path)
{
	struct tokens: std::ctype<char>
	{
	    tokens(): std::ctype<char>(get_table()) {}

	    static std::ctype_base::mask const* get_table()
	    {
	        typedef std::ctype<char> cctype;
	        static const cctype::mask *const_rc= cctype::classic_table();

	        static cctype::mask rc[cctype::table_size];
	        std::memcpy(rc, const_rc, cctype::table_size * sizeof(cctype::mask));

	        rc[' '] = std::ctype_base::space;
	        return &rc[0];
	    }
	};

	std::stringstream ss(path);
	// NOTE: std::locale is responsible for calling delete on the new facet
	ss.imbue(std::locale(std::locale(), new tokens()));
	std::istream_iterator<std::string> begin(ss);
	std::istream_iterator<std::string> end;
	std::vector<std::string> pathParts(begin, end);

	const Json::Value* currentValue = &serializedState;

	for(auto pathPart : pathParts) {
		if(currentValue->isObject() && currentValue->isMember(pathPart)) {
			currentValue = &((*currentValue)[pathPart]);
		} else {
			Json::Value::UInt index = strtol(pathPart.c_str(), NULL, 10);
			if (currentValue->isArray() && index < currentValue->size()) {
				currentValue = &((*currentValue)[index]);
			} else {
				return std::nullopt;
			}
		}
	}

	// Found the object in question
	if(currentValue->isObject() && currentValue->isMember("__pointer")) {
		Serializable* object = (Serializable*)(*currentValue)["__pointer"].asLargestUInt();

		return object;
	} else {
		return std::nullopt;
	}
}

std::optional<std::string> getObjectPath(const Json::Value& serializedState, const Serializable* object)
{
	if(serializedState.isObject() && serializedState.isMember("__pointer"))
	{
		Serializable* objectCandidate = (Serializable*)serializedState["__pointer"].asLargestUInt();

		if(objectCandidate == object) {
			return "";
		}
	}

	if(serializedState.isObject()) {
		for(auto key : serializedState.getMemberNames()) {
			auto res = getObjectPath(serializedState[key], object);

			if(res.has_value()) {
				return key + " " + res.value();
			}
		}
	}

	if(serializedState.isArray()) {
		for(Json::Value::UInt i = 0; i < serializedState.size(); ++i) {
			auto res = getObjectPath(serializedState[i], object);

			if(res.has_value()) {
				std::stringstream ss;

				ss << i;

				return ss.str() + " " + res.value();
			}
		}

	}

	return std::nullopt;
}

Json::Value serializeRoot(const Serializable* root) {
	if(!root) {
		return Json::Value();
	}

	ObjectIdMapper objectIdMapper;

	DefaultSerializer serializer(objectIdMapper);

	return serializer.serialize(*root);
}


}
