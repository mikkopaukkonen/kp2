#include "object.hpp"

#include <algorithm>

namespace Game
{

std::shared_ptr<Object::Listener> Object::addListener(Object::Listener listener)
{
	auto sharedListener = std::make_shared<Listener>(listener);

	listeners.push_back(std::weak_ptr<Listener>(sharedListener));

	return sharedListener;
}

void Object::save(Json::Value& value) const
{
	(void)value;
}

Object& Object::operator=(const Object&)
{
	return *this;
}

Object::Object(const Json::Value& value):
	Serializable(value)
{

}

void Object::changed()
{
	for(auto listener : listeners) {
		if(auto sharedListener = listener.lock()) {
			sharedListener->onChange(this);
		}
	}
}

};
