#include "vehicle.hpp"

#include <set>

namespace Game
{

const std::string& Vehicle::getName() const
{
	return name;
}

const std::string& Vehicle::getInfo() const
{
	return info;
}

float Vehicle::getMass() const
{
	return 0;
}

int Vehicle::getPrice() const
{
	return price;
}

int Vehicle::getYear() const
{
	return year;
}

const std::string& Vehicle::getImageName() const
{
	return imageName;
}

const Container<Part>& Vehicle::getAttachedParts() const
{
	return parts;
}

Part* Vehicle::getAttachedPart(const Part& attachedTo, const Slot& slot) const
{
	auto it = attachments.find(PartAttachment(&attachedTo, slot.getName()));

	if (it != attachments.end()) {
		return it->second;
	}

	return nullptr;
}

template <class B>
void createAttachmentsForPartContainer(PartContainer& attachTo, Part& part, B backInserter) {
	for(auto slotNameAndSlot: attachTo.getSlots()) {
		Slot& slot = slotNameAndSlot.second;

		if(slot.canAttachPart(part)) {
			backInserter = Vehicle::PartAttachment(&attachTo, slot.getName());
		}
	}
}

void Vehicle::attachPart(std::unique_ptr<Part>& partPtr)
{
	Part& part = *partPtr;

	std::set<PartAttachment> possibleAttachments;

	for(Part& attachTo: parts) {
		createAttachmentsForPartContainer(attachTo, part, std::inserter(possibleAttachments, possibleAttachments.end()));
	}

	createAttachmentsForPartContainer(*this, part, std::inserter(possibleAttachments, possibleAttachments.end()));

	std::set<PartAttachment> existingAttachments;

	std::transform(attachments.begin(), attachments.end(),
	    std::inserter(existingAttachments, existingAttachments.end()),
	    [](auto pair){ return pair.first; });

	std::vector<PartAttachment> newAttachments;

	std::set_difference(possibleAttachments.begin(), possibleAttachments.end(), existingAttachments.begin(), existingAttachments.end(), std::back_inserter(newAttachments));

	if(newAttachments.size() > 0) {
		parts.add(std::move(partPtr));
	}

	for(auto attachment : newAttachments) {
		attachments.insert(std::make_pair(attachment, &part));
	}
}

void Vehicle::applyPropertiesOf(const Vehicle& vehicle)
{
	name = vehicle.getName();
	price = vehicle.getPrice();
	year = vehicle.getYear();
	info = vehicle.getInfo();
	imageName = vehicle.getImageName();
}

Vehicle::Vehicle(const std::string& name, int price, int year, const std::string& info, const std::string& imageName):
	name(name),
	price(price),
	year(year),
	info(info),
	imageName(imageName),
        parts(),
        chassisSlot("chassis", *this)
{
	registerSlot(chassisSlot);
}

Vehicle::Vehicle(const Json::Value& value, Deserializer& deserializer):
	PartContainer(value),
	name(value["name"].asString()),
	price(value["price"].asUInt()),
	year(value["year"].asUInt()),
	info(value["info"].asString()),
	imageName(value["imageName"].asString()),
	parts(value["parts"], deserializer),
        chassisSlot("chassis", *this)
{
	registerSlot(chassisSlot);
}

void Vehicle::save(Json::Value& value) const
{
	PartContainer::save(value);

	value["type"] = "Vehicle";

	value["name"] = name;
	value["price"] = price;
	value["year"] = year;
	value["info"] = info;
	value["imageName"] = imageName;

	parts.save(value["parts"]);
}

}
