#ifndef GAME_DESERIALIZER_HPP
#define GAME_DESERIALIZER_HPP

#include <memory>
#include <functional>
#include <typeinfo>

#include "serializable.hpp"

namespace Game {

class Deserializer
{
	public:
		virtual std::unique_ptr<Serializable> deserialize(const Json::Value&) = 0;
		virtual Serializable* deserializeAsReference(const Json::Value&) = 0;

		template<class T>
		std::unique_ptr<T> deserializeExpectingType(const Json::Value& v)
		{
			std::unique_ptr<Serializable> basePointer = deserialize(v);
			T *tmp = dynamic_cast<T*>(basePointer.get());
			std::unique_ptr<T> derivedPointer;

			if(tmp == nullptr) {
				throw std::bad_cast();
			}

			basePointer.release();
			derivedPointer.reset(tmp);

			return derivedPointer;
		}

		template<class T>
		T* deserializeExpectingPointer(const Json::Value& v)
		{
			Serializable* basePointer = deserializeAsReference(v);

			if(basePointer == nullptr) {
				return nullptr;
			}

			T* derivedPointer = dynamic_cast<T*>(basePointer);

			if(derivedPointer == nullptr) {
				throw std::bad_cast();
			}

			return derivedPointer;
		}

		template<class T>
		using base_type = typename std::remove_const<T>::type;

	protected:
		Deserializer() = default;

		virtual std::unique_ptr<Serializable> instantiate(std::string type, const Json::Value& value) = 0;

};

}

#endif // GAME_DESERIALIZER_HPP
