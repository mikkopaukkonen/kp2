#ifndef GAME_CYLINDERBLOCK_HPP
#define GAME_CYLINDERBLOCK_HPP

#include "part.hpp"
#include "cylinderhead.hpp"
#include "crankshaft.hpp"

#include <string>

namespace Game
{

class CylinderBlock : public Part
{
	public:
		int getCylinderCount() const;
		double getBore() const;
		double getHeight() const;

		void applyPropertiesOf(const CylinderBlock& cylinderBlock);

		CylinderBlock(int cylinderCount, double bore, double height);
		CylinderBlock(const Json::Value& value);
		virtual void save(Json::Value& value) const;

	private:
		template <class T, class U>
		class MatchingCylinderCountSlot: public TypedSlot<T, U>{
			using TypedSlot<T, U>::TypedSlot;

			public:
				bool canAttachPart(const T *t, const U *u) const {
					return t->getCylinderCount() == u->getCylinderCount();
				}
		};

		int cylinderCount;
		double bore;
		double height;

		MatchingCylinderCountSlot<CylinderHead, CylinderBlock> cylinderHeadSlot;
		MatchingCylinderCountSlot<Crankshaft, CylinderBlock> crankshaftSlot;
};

};

#endif
