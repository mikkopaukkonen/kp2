#ifndef GAME_CHASSIS_HPP
#define GAME_CHASSIS_HPP

#include "part.hpp"
#include "cylinderblock.hpp"

namespace Game
{

class Chassis : public Part
{
	public:
		class Dimensions
		{
			public:
				double length;
				double width;
				double height;
				double wheelbase;
				double axleTrack;
		};

		class EngineConstraints
		{
			public:
				int maxVolume;
				int maxCylinderCount;
		};

		float getDragCoefficient() const;
		const Dimensions& getDimensions() const;
		const EngineConstraints& getEngineConstraints() const;

		void applyPropertiesOf(const Chassis& chassis);

		Chassis(float mass, float dragCoefficient, const Dimensions&, const EngineConstraints&);
		Chassis(const Json::Value&);
		virtual void save(Json::Value& value) const;

	private:
		double dragCoefficient;
		Dimensions dimensions;
		EngineConstraints engineConstraints;

		class: public TypedSlot<CylinderBlock, Chassis>{
			using TypedSlot<CylinderBlock, Chassis>::TypedSlot;

			public:
				bool canAttachPart(const CylinderBlock *part, const Chassis *chassis) const {
					return chassis->getEngineConstraints().maxCylinderCount >= part->getCylinderCount();
				}

		} cylinderBlockSlot;
};

};

#endif

