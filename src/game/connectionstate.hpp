#ifndef GAME_CONNECTIONSTATE_HPP
#define GAME_CONNECTIONSTATE_HPP

#include "state.hpp"
#include "session.hpp"

namespace Game {

class ConnectionState: public Object
{
	public:
		State& getState();
		Session& getSession();

		Json::Value serialize(Serializer &) const;

		ConnectionState(State& state, std::unique_ptr<Session> session);
		ConnectionState(const Json::Value& serialized, Deserializer& deserializer);

	private:
		std::unique_ptr<State> deserializedState;
		State* state;

		std::unique_ptr<Session> session;
};

}

#endif // CONNECTIONSTATE_HPP
