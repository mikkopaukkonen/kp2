#ifndef GAME_OBJECT_HPP
#define GAME_OBJECT_HPP

#include "serializable.hpp"

#include <memory>
#include <functional>

namespace Game
{

class Object : public Serializable
{
	public:
		class Listener
		{
			public:
				void onChange(Object* object) {
					onChangeHandler(object);
				}

				Listener(std::function<void(Object*)> onChangeHandler):
				        onChangeHandler(onChangeHandler)
				{}

			private:
				std::function<void(Object*)> onChangeHandler;

		};

		std::shared_ptr<Listener> addListener(Listener listener);

		void save(Json::Value &) const;

		Object() = default;
		Object(const Json::Value&);
		virtual ~Object() = default;

	protected:
		Object(const Object&) = default;
		Object& operator=(const Object&);

		void changed();

	private:
		std::vector<std::weak_ptr<Listener>> listeners;
};




};

#endif
