#include "connectionstate.hpp"

namespace Game
{

ConnectionState::ConnectionState(State& state, std::unique_ptr<Session> session):
        state(&state),
	session(std::move(session))
{

}

ConnectionState::ConnectionState(const Json::Value& serialized, Game::Deserializer& deserializer):
        deserializedState(deserializer.deserializeExpectingType<State>(serialized["state"])),
	state(deserializedState.get()),
	session(deserializer.deserializeExpectingType<Session>(serialized["session"]))
{

}

State& ConnectionState::getState()
{
	return *state;
}

Session& ConnectionState::getSession()
{
	return *session;
}

Json::Value ConnectionState::serialize(Serializer& serializer) const
{
	Json::Value serialized = Object::serialize(serializer);

	serialized["type"] = "ConnectionState";
	serialized["state"] = serializer.serialize(*state);
	serialized["session"] = serializer.serialize(*session);

	return serialized;
}

}
