#include "slot.hpp"

namespace Game {

const std::string& Slot::getName() const
{
	return name;
}

bool Slot::canAttachPart(const Part& part) const
{
	(void)part;

	return false;
}

Slot::Slot(const std::string& name):
        name(name)
{

}

}
