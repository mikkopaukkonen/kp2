#include "identifier.hpp"

#include <cryptopp/cryptlib.h>
#include <cryptopp/osrng.h>
#include <cryptopp/hex.h>
#include <cryptopp/sha.h>

std::string generateIdentifier()
{
	using namespace CryptoPP;

	AutoSeededRandomPool prng;

	uint8_t key[32];

	prng.GenerateBlock(key, sizeof(key));

	std::string clientId;

	StringSource(key, 32, true, new HexEncoder(new StringSink(clientId)));

	return clientId;
}
