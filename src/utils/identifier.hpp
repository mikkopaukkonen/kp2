#ifndef UTILS_IDENTIFIER_HPP
#define UTILS_IDENTIFIER_HPP

#include <string>

std::string generateIdentifier();

#endif // IDENTIFIER_HPP
