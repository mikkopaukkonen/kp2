#ifndef SOUNDPLAYER_HPP
#define SOUNDPLAYER_HPP

#include <cstring>
#include <string>
#include <audiere.h>

const std::string SOUND_DIRECTORY = "data/sounds/";
const std::string SOUND_SETTINGS = "cfg/sound.cfg";

class SoundPlayer
{
	public:
		void play(const std::string& file);

		void setVolume(int volume);
		int getVolume();

		SoundPlayer();
		~SoundPlayer();

	private:
		audiere::AudioDevicePtr device;
		audiere::OutputStreamPtr sound;
		float volume;

		void loadSettings();
		void saveSettings();
};

#endif

