#include "soundplayer.hpp"

#include <fstream>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <cmath>

#include "json/json.h"

void SoundPlayer::setVolume(int volume)
{
	if(volume > 100)
		volume = 100;

	float coeff = volume / 100.0;

	if(sound)
	{
		sound->setVolume(coeff * coeff);
	}

	this->volume = volume;
}

int SoundPlayer::getVolume()
{
	return volume;
}

SoundPlayer::SoundPlayer():
	device(0),
	sound(0),
	volume(100)
{
	try
	{
		loadSettings();
	}
	catch(...)
	{

	}	

	device = audiere::OpenDevice();
}

SoundPlayer::~SoundPlayer()
{
	try
	{
		saveSettings();
	}
	catch(...)
	{

	}
}

void SoundPlayer::play(const std::string& file)
{
	if(!device)
		return;

	sound = audiere::OpenSound(device, (SOUND_DIRECTORY + file).c_str());

	if(!sound)
		return;

	setVolume(volume);
	sound->play();
}

void SoundPlayer::loadSettings()
{
	Json::Value settings;

	std::ifstream(SOUND_SETTINGS) >> settings;

	volume = settings["volume"].asDouble();
}

void SoundPlayer::saveSettings()
{
	Json::Value settings;

	settings["volume"] = volume;

	std::ofstream(SOUND_SETTINGS) << settings;
}
