#include "parttablemodel.hpp"

#include <typeinfo>

std::string PartTableModel::NameField::getHeader() const
{
	return "Nimi";
}

QVariant PartTableModel::NameField::getData(Game::Part* v) const
{
	return QVariant(typeid(*v).name());
}

QVariant PartTableModel::NameField::getDecoration(Game::Part*) const
{
	return QVariant();
}

PartTableModel::NameField::NameField(ObjectTableModel* parent):
	Field(parent)
{

}

std::string PartTableModel::PriceField::getHeader() const
{
	return "Hinta";
}

QVariant PartTableModel::PriceField::getData(Game::Part* v) const
{
	return QVariant(QString::number(v->getPrice()));
}

QVariant PartTableModel::PriceField::getDecoration(Game::Part*) const
{
	return QVariant();
}

PartTableModel::PriceField::PriceField(ObjectTableModel* parent):
	Field(parent)
{

}

PartTableModel::PartTableModel(const Game::Container<Game::Part>& dataSource):
	ObjectTableModel(dataSource),
	name(this),
	price(this)
{

}
