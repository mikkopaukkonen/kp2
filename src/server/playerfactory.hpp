#ifndef SERVER_PLAYERFACTORY_HPP
#define SERVER_PLAYERFACTORY_HPP

#include "game/player.hpp"

namespace Server {

class PlayerFactory
{
	public:
		std::unique_ptr<Game::Player> create();

		PlayerFactory(int startingMoney);

	private:
		Game::Player templatePlayer;
};

}

#endif // PLAYERFACTORY_HPP
