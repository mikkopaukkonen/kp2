#include "session.hpp"

#include "game/remotecall.hpp"

namespace Server {

void Session::claimPlayer(std::string identifier)
{
	auto& players = gameState.getPlayers();

	auto playerIt = std::find_if(players.begin(), players.end(), [&](Game::Player& player){
		return player.isIdentifiedBy(identifier);
	});

	Game::Player* claimedPlayer;

	if(playerIt != players.end()) {
		claimedPlayer = &(*playerIt);
	} else {
		claimedPlayer = gameState.addPlayer(playerFactory.create());
		claimedPlayer->setIdentifier(identifier);
	}

	this->setPlayer(claimedPlayer);
}

void Session::setPlayer(Game::Player* player)
{
	Game::Session::setPlayer(player);

	remoteCallDistributor.makeRemoteCall(Game::SessionSetPlayerRemoteCall(this, player));
}

Session::Session(Game::State& gameState, PlayerFactory& playerFactory, RemoteCallDistributor& remoteCallDistributor):
        gameState(gameState),
        playerFactory(playerFactory),
        remoteCallDistributor(remoteCallDistributor)
{

}

}
