#include "playerfactory.hpp"

#include "game/defaultserializer.hpp"
#include "game/player.hpp"

namespace Server {

std::unique_ptr<Game::Player> PlayerFactory::create()
{
	Game::ObjectIdMapper objectIdMapper;
	Game::DefaultSerializerDeserializer serializer(objectIdMapper);

	return serializer.deserializeExpectingType<Game::Player>(serializer.serialize(templatePlayer));
}

PlayerFactory::PlayerFactory(int startingMoney)
{
	templatePlayer.addMoney(startingMoney);
}

}
