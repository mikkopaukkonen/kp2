#include "deserializerfactory.hpp"

namespace Server {

Game::DefaultSerializerDeserializer DeserializerFactory::create(Game::ObjectIdMapper& objectIdMapper) const
{
	return Game::DefaultSerializerDeserializer(objectIdMapper);
}

}
