#ifndef SERVER_DESERIALIZER_HPP
#define SERVER_DESERIALIZER_HPP

#include "game/defaultserializer.hpp"

struct RemoteCallDistributor;

namespace Server
{

class Deserializer : public Game::DefaultDeserializer
{
	public:
		Deserializer(Game::ObjectIdMapper& objectIdMapper, RemoteCallDistributor& remoteCallDistributor);

	protected:
		std::unique_ptr<Game::Serializable> instantiate(std::string type, const Json::Value &value);

	private:
		RemoteCallDistributor& remoteCallDistributor;

};

}

#endif
