#include "connectionfactory.hpp"

#include "game/deserializer.hpp"
#include "game/serializer.hpp"
#include "game/player.hpp"

#include "session.hpp"
#include "connection.hpp"

namespace Server {

std::unique_ptr<Connection> ConnectionFactory::create(Net::ClientSocket& socket)
{
	std::unique_ptr<Game::ConnectionState> connectionState = std::make_unique<Game::ConnectionState>(gameState, std::make_unique<Server::Session>(gameState, playerFactory, remoteCallDistributor));
	std::unique_ptr<Connection> connection = std::make_unique<Connection>(socket, std::move(connectionState), deserializerFactory);

	return connection;
}

ConnectionFactory::ConnectionFactory(Game::State& gameState, DeserializerFactory& deserializerFactory, PlayerFactory& playerFactory, RemoteCallDistributor& remoteCallDistributor):
        gameState(gameState),
        deserializerFactory(deserializerFactory),
        playerFactory(playerFactory),
        remoteCallDistributor(remoteCallDistributor)
{

}

}
