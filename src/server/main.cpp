#include <iostream>
#include <string>

#include "utils/outputredirector.hpp"
#include "utils/commandlineparser.hpp"
#include "utils/timer.hpp"

#include "net/serversocket.hpp"

#include "connectionmanager.hpp"
#include "game/deserializer.hpp"
#include "game/state.hpp"
#include "connectionfactory.hpp"
#include "playerfactory.hpp"
#include "utils/identifier.hpp"

void startServer(int argc,char** argv)
{
	std::cout<<"Kiihdytyspeli 2 Server"<<std::endl;

	CommandLineParser commandLineParser(argc,argv);

	std::string configPath="cfg/server.cfg";

	commandLineParser.getValue("config",configPath);

	std::cout<<"Reading config from \""<<configPath<<"\"."<<std::endl;

	Json::Value config;

	std::ifstream configStream(configPath);

	configStream >> config;

	unsigned int port = config["port"].asUInt();
	int connectionLimit = config["connectionLimit"].asUInt();
	bool quitWhenEmpty = config["quitWhenEmpty"].asBool();
	bool isLocal = config["isLocal"].asBool();
	bool generateNewGameIdentifier = config.get("generateNewGameIdentifier", false).asBool();
	std::string statePath = config.get("stateFileName", "gamedata/beginstate.kp2").asString();

	std::cout<<"Reading game state from \"" << statePath << "\"."<<std::endl;

	Json::Value state;
	std::ifstream stateStream(statePath);
	stateStream >> state;

	RemoteCallDistributor remoteCallDistributor;

	Game::ObjectIdMapper objectIdMapper;
	Server::Deserializer deserializer(objectIdMapper, remoteCallDistributor);
	Game::DefaultSerializer serializer(objectIdMapper);

	std::unique_ptr<Game::State> gameStatePtr = deserializer.deserializeExpectingType<Game::State>(state);
	Game::State& gameState = *(gameStatePtr.get());

	if(gameState.getIdentifier().size() == 0 || generateNewGameIdentifier) {
		gameState.setIdentifier(generateIdentifier());
	}

	Server::PlayerFactory playerFactory(5000);
	Server::DeserializerFactory deserializerFactory;

	Server::ConnectionFactory connectionFactory(gameState, deserializerFactory, playerFactory, remoteCallDistributor);

	std::string hostName;

	if(isLocal)
		hostName = "127.0.0.1";
	else
		hostName = "0.0.0.0";

	Net::ServerSocket serverSocket;

	serverSocket.open(hostName, port);

	ConnectionManager connectionManager(serverSocket, playerFactory, connectionFactory);

	using namespace std::placeholders;

	remoteCallDistributor.makeRemoteCall = std::bind(&ConnectionManager::makeRemoteCallForAll, &connectionManager, _1);

	bool playersHaveJoined = false;
	bool running = true;

	while(running)
	{
		connectionManager.processConnections();

		if(connectionManager.getConnectionCount() > 0)
			playersHaveJoined = true;

		if(connectionManager.getConnectionCount() > connectionLimit) {
			std::cerr << "Connection limit exceeded!" << std::endl;
		}

		if(quitWhenEmpty && connectionManager.getConnectionCount() == 0 && playersHaveJoined)
			running = false;
	}

	Json::Value savedState = serializer.serialize(gameState);
	std::ofstream("gamedata/saves/autosave.kp2") << savedState;
}

int main(int argc,char** argv)
{
	try
	{
		startServer(argc,argv);
	}
	catch(std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

	std::cout << "Server shutting down." <<std::endl;
}

