#ifndef SERVER_DESERIALIZERFACTORY_HPP
#define SERVER_DESERIALIZERFACTORY_HPP

#include "game/state.hpp"
#include "game/objectidmapper.hpp"

#include "deserializer.hpp"

namespace Server {

class DeserializerFactory
{
	public:
		Game::DefaultSerializerDeserializer create(Game::ObjectIdMapper& objectIdMapper) const;
};

}

#endif // SERVER_DESERIALIZERFACTORY_HPP
