#include "connectionmanager.hpp"

#include "game/player.hpp"
#include "utils/demangle.hpp"
#include "protocol/protocol.hpp"

#include "session.hpp"

#include <stdexcept>
#include <iostream>

void ConnectionManager::processConnections()
{
	Net::SocketActivity activity = socketSet.waitForActivity();

	if(activity.socket == &serverSocket)
	{
		acceptConnection();
	}
	else
	{
		Net::ClientSocket* socket = dynamic_cast<Net::ClientSocket*>(activity.socket);

		try
		{
			if(activity.canRead)
				readFromSocket(socket);

			writeToSocket(socket);
		}
		catch(Net::ConnectionClosedException& e)
		{
			std::cerr << "Connection closed: ";
			std::cerr << e.getMessage();
			std::cerr << std::endl;

			closeConnectionBySocket(socket);
		}
	}
}

int ConnectionManager::getConnectionCount()
{
	return connectionsBySocket.size();
}

Connection& ConnectionManager::getConnectionByIndex(int index)
{
	auto i = connectionsBySocket.begin();

	while(index > 0)
	{
		if(i == connectionsBySocket.end())
			throw std::runtime_error("No connection for index");

		i++;

		index--;
	}

	return *(i->second.get());
}

void ConnectionManager::acceptConnection()
{
	using namespace std::placeholders;

	Net::ClientSocket newSocket;

	std::cout << "New connection" << std::endl;

	serverSocket.accept(newSocket);

	sockets.push_back(newSocket);

	Net::ClientSocket* socket = &sockets.back();

	std::unique_ptr<Connection> connection = connectionFactory.create(*socket);

	connectionsBySocket.insert(std::make_pair(socket, std::move(connection)));
	socketSet.add(socket);
}

void ConnectionManager::readFromSocket(Net::ClientSocket* socket)
{
	try
	{
		connectionsBySocket.at(socket)->processReceivedData();
	}
	catch(Game::Exception& e)
	{
		std::cerr << "Exception when processing received data: " << demangleName(typeid(e).name()) << std::endl;
	}
}

void ConnectionManager::writeToSocket(Net::ClientSocket* socket)
{
	connectionsBySocket.at(socket)->writeBufferedData();
}

void ConnectionManager::closeConnectionBySocket(Net::ClientSocket* socket)
{
	connectionsBySocket.erase(socket);

	socketSet.remove(socket);

	std::list<Net::ClientSocket>::iterator i;

	for(i = sockets.begin(); i != sockets.end(); ++i)
	{
		Net::ClientSocket* s = &(*i);

		if(socket == s)
		{
			sockets.erase(i);
			break;
		}
	}
}

void ConnectionManager::makeRemoteCallForAll(const Game::RemoteCall& remoteCall)
{
	for(auto& pair : connectionsBySocket) {
		pair.second->makeRemoteCall(remoteCall);
	}
}

ConnectionManager::ConnectionManager(Net::ServerSocket& serverSocket, Server::PlayerFactory& playerFactory, Server::ConnectionFactory& connectionFactory):
	serverSocket(serverSocket),
	playerFactory(playerFactory),
        connectionFactory(connectionFactory)
{
	socketSet.add(&serverSocket);
}

