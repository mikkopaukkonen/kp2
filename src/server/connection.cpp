#include "connection.hpp"

#include <sstream>
#include <iostream>

#include "net/clientsocket.hpp"

#include "protocol/protocol.hpp"

#include "exceptions.hpp"
#include "deserializer.hpp"
#include "game/remotecall.hpp"

std::unique_ptr<Json::StreamWriter> createJsonFastWriter();

void Connection::processReceivedData()
{
	int bytesRead;

	while((bytesRead = socket.read(scrapBuffer, BUFFERSIZE)) > 0)
		receiveBuffer.append(scrapBuffer, bytesRead);

	if(bytesRead == -1)
		return;

	while(1)
	{
		Net::Packet packet;

		try
		{
			packet.readFromBuffer(receiveBuffer);

			if(packet.getType() == Protocol::REMOTE_INVOCATION)
			{
				std::string s;
				packet >> s;
				std::stringstream ss(s);

				Json::Value call;
				ss >> call;

				try {
					auto deserializer = deserializerFactory.create(objectIdMapper);

					std::unique_ptr<Game::RemoteCall> remoteCall = deserializer.deserializeExpectingType<Game::RemoteCall>(call);

					remoteCall->callOnLocalObject();
				} catch (std::exception& e) {
					std::cerr << "Error while processing remote call " << e.what() <<  std::endl;
				}
			}
		}
		catch(Net::EndOfDataException)
		{
			break;
		}
		catch(Game::ObjectIdMapper::UnresolvedReferenceError)
		{
			std::cout << "Object mapping failed" << std::endl;
		}
		catch(...)
		{
			std::cout << "Caught exception in processReceivedData" << std::endl;
		}
	}

	writeBufferedData();
}

void Connection::writeBufferedData()
{
	int written;

	while((written = socket.write(sendBuffer.c_str(), sendBuffer.size())) > 0)
			sendBuffer.erase(0, written);
}

void Connection::writePacket(const Net::Packet& packet)
{
	std::string str = packet.getString();

	sendBuffer.append(str.c_str(), str.size());
}

void Connection::makeRemoteCall(const Game::RemoteCall& remoteCall)
{
	Json::Value call = deserializerFactory.create(objectIdMapper).serialize(remoteCall);

	Net::Packet packet;
	packet.setType(Protocol::REMOTE_INVOCATION);

	std::stringstream ss;

	auto jsonWriter = createJsonFastWriter();
	jsonWriter->write(call, &ss);

	packet << ss.str();

	writePacket(packet);
	writeBufferedData();
}

Connection::Connection(Net::ClientSocket& socket, std::unique_ptr<Game::ConnectionState> connectionState, Server::DeserializerFactory& deserializerFactory):
        socket(socket),
        connectionState(std::move(connectionState)),
        deserializerFactory(deserializerFactory),
	objectIdMapper("", this->connectionState.get())
{
	auto serializer = deserializerFactory.create(this->objectIdMapper);

	Json::Value state = serializer.serialize(*this->connectionState);

	std::cout << "Sending connection state:" << std::endl;
	std::cout << state << std::endl;

	Net::Packet packet;
	packet.setType(Protocol::GAME_STATE);

	std::stringstream ss;

	auto jsonWriter = createJsonFastWriter();
	jsonWriter->write(state, &ss);

	packet << ss.str();

	writePacket(packet);
	writeBufferedData();
}

std::unique_ptr<Json::StreamWriter> createJsonFastWriter() {
	Json::StreamWriterBuilder builder;
	builder["commentStyle"] = "None";
	builder["indentation"] = "";
	return std::unique_ptr<Json::StreamWriter>(builder.newStreamWriter());
}
