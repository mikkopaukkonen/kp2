#include "deserializer.hpp"

#include "state.hpp"

#include "connectionmanager.hpp"

namespace Server
{

Deserializer::Deserializer(Game::ObjectIdMapper& objectIdMapper, RemoteCallDistributor& remoteCallDistributor):
	Game::DefaultDeserializer(objectIdMapper),
        remoteCallDistributor(remoteCallDistributor)
{

}

std::unique_ptr<Game::Serializable> Deserializer::instantiate(std::string type, const Json::Value& value)
{
	if(type == "State")
		return std::make_unique<State>(value, *this, remoteCallDistributor);

	return Game::DefaultDeserializer::instantiate(type, value);
}

}
