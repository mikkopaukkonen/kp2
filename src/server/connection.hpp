#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include "net/packet.hpp"
#include "net/clientsocket.hpp"
#include "deserializerfactory.hpp"
#include "game/connectionstate.hpp"
#include "game/remotecall.hpp"
#include <string>


const int BUFFERSIZE=512;

class Connection
{
	public:
		void processReceivedData();
		void writeBufferedData();

		void writePacket(const Net::Packet& packet);
		void makeRemoteCall(const Game::RemoteCall& remoteCall);

		Connection(Net::ClientSocket& socket, std::unique_ptr<Game::ConnectionState> connectionState, Server::DeserializerFactory& deserializerFactory);

	private:
		void addIds(Json::Value &value);

		std::string receiveBuffer;
		std::string sendBuffer;
		char scrapBuffer[BUFFERSIZE];

		Net::ClientSocket& socket;
		std::unique_ptr<Game::ConnectionState> connectionState;
		Server::DeserializerFactory& deserializerFactory;
		Game::ObjectIdMapper objectIdMapper;
};

#endif // CONNECTION_HPP

