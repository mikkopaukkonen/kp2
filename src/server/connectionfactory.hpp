#ifndef SERVER_CONNECTIONFACTORY_HPP
#define SERVER_CONNECTIONFACTORY_HPP

#include "net/clientsocket.hpp"

#include "connection.hpp"
#include "deserializerfactory.hpp"
#include "playerfactory.hpp"

namespace Server {

class ConnectionFactory
{
	public:
		std::unique_ptr<Connection> create(Net::ClientSocket& socket);

		ConnectionFactory(Game::State& gameState, DeserializerFactory& deserializerFactory, PlayerFactory& playerFactory, RemoteCallDistributor& remoteCallDistributor);
	private:
		Game::State& gameState;
		DeserializerFactory& deserializerFactory;
		PlayerFactory& playerFactory;
		RemoteCallDistributor& remoteCallDistributor;
};

}

#endif // PLAYERFACTORY_HPP
