#ifndef SERVER_CONNECTIONMANAGER_HPP
#define SERVER_CONNECTIONMANAGER_HPP

#include <map>
#include <list>

#include "game/state.hpp"
#include "game/remotecall.hpp"
#include "connection.hpp"
#include "playerfactory.hpp"
#include "connectionfactory.hpp"

#include "net/socketset.hpp"
#include "net/clientsocket.hpp"
#include "net/serversocket.hpp"

struct RemoteCallDistributor {
	std::function<void(const Game::RemoteCall&)> makeRemoteCall;
};

// This class handles the mapping of sockets to connections
class ConnectionManager
{
	public:
		void processConnections();

		int getConnectionCount();
		Connection& getConnectionByIndex(int index);

		void makeRemoteCallForAll(const Game::RemoteCall& remoteCall);

		ConnectionManager(Net::ServerSocket&, Server::PlayerFactory&, Server::ConnectionFactory& connectionFactory);

	private:
		void acceptConnection();
		void readFromSocket(Net::ClientSocket* socket);
		void writeToSocket(Net::ClientSocket* socket);
		void closeConnectionBySocket(Net::ClientSocket* socket);

		Net::ServerSocket& serverSocket;
		Server::PlayerFactory& playerFactory;
		Server::ConnectionFactory& connectionFactory;

		std::map<Net::ClientSocket*, std::unique_ptr<Connection>> connectionsBySocket;

		std::list<Net::ClientSocket> sockets;

		Net::SocketSet socketSet;

};

#endif

