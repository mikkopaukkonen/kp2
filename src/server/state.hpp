#ifndef SERVER_STATE_HPP
#define SERVER_STATE_HPP

#include "game/state.hpp"
#include "connectionmanager.hpp"

namespace Server {

class State : public Game::State
{
	public:
		virtual Game::Player* addPlayer(std::unique_ptr<Game::Player> player) override;

		State(RemoteCallDistributor& remoteCallDistributor);
		State(const Json::Value& serialized, Deserializer& deserializer, RemoteCallDistributor& remoteCallDistributor);

	private:
		RemoteCallDistributor& remoteCallDistributor;
};

}

#endif // STATE_HPP
