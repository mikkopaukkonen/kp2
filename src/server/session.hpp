#ifndef SERVER_SESSION_HPP
#define SERVER_SESSION_HPP

#include "game/session.hpp"
#include "connectionmanager.hpp"
#include "playerfactory.hpp"

#include <functional>

namespace Server {

class Session: public Game::Session
{
	public:
		virtual void claimPlayer(std::string identifier);

		virtual void setPlayer(Game::Player* player) override;

		Session(Game::State& gameState, PlayerFactory& playerFactory, RemoteCallDistributor& remoteCallDistributor);

	private:
		Game::State& gameState;
		PlayerFactory& playerFactory;
		RemoteCallDistributor& remoteCallDistributor;
};

}

#endif // SESSION_HPP
