#include "state.hpp"

#include "game/remotecall.hpp"

namespace Server {

Game::Player* State::addPlayer(std::unique_ptr<Game::Player> player)
{
	Game::Player* rawPlayer = Game::State::addPlayer(std::move(player));

	remoteCallDistributor.makeRemoteCall(Game::StateAddPlayerRemoteCall(this, rawPlayer));

	return rawPlayer;
}

State::State(RemoteCallDistributor& remoteCallDistributor):
        remoteCallDistributor(remoteCallDistributor)
{

}

State::State(const Json::Value& serialized, Deserializer& deserializer, RemoteCallDistributor& remoteCallDistributor):
        Game::State(serialized, deserializer),
        remoteCallDistributor(remoteCallDistributor)
{

}

}
