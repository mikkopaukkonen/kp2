#ifndef EDITOR_TYPESPECIFICFORMFACTORY_H
#define EDITOR_TYPESPECIFICFORMFACTORY_H

#include <QWidget>

#include "typespecificform.hpp"
#include "mainwindow.hpp"
#include "game/object.hpp"

namespace Editor
{

class TypeSpecificFormFactory
{
	public:
		TypeSpecificForm* createFormFor(Game::Object* object);

		TypeSpecificFormFactory(MainWindow& mainWindow);

	private:
		MainWindow& mainWindow;

};

};

#endif
