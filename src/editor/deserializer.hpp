#ifndef EDITOR_DESERIALIZER_HPP
#define EDITOR_DESERIALIZER_HPP

#include <string>
#include <memory>
#include "game/object.hpp"
#include "game/defaultserializer.hpp"

namespace Editor
{

class Deserializer
{
	public:
		template<class T = Game::Serializable>
		std::unique_ptr<T> createObject(const std::string& className) {
			Game::ObjectIdMapper objectIdMapper;
			Game::DefaultDeserializer deserializer(objectIdMapper);

			return deserializer.deserializeExpectingType<T>(getObjectTemplate(className));
		}

	private:
		Json::Value getObjectTemplate(const std::string& className);
};

};

#endif
