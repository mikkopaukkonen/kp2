#ifndef EDITOR_VEHICLEFORM_HPP
#define EDITOR_VEHICLEFORM_HPP

#include <memory>

#include "typespecificformfactory.hpp"
#include "typespecificform.hpp"
#include "mainwindow.hpp"
#include "ui_vehicleform.h"
#include "game/vehicle.hpp"
#include "models/parttablemodel.hpp"
#include <QTreeWidgetItem>

namespace Editor
{

class VehicleForm : public TypeSpecificForm, private Ui::VehicleForm
{
	Q_OBJECT

	public:
		virtual void updateOriginal();

		explicit VehicleForm(Game::Vehicle* vehicle, MainWindow& mainWindow, TypeSpecificFormFactory& typeSpecificFormFactory, QWidget* parent = 0);

	private:
		void openEditor(Game::Object *object);
		void setItemIcon(QTreeWidgetItem* item);
		void addSlotsFor(Game::Part* part, QTreeWidgetItem *item = 0);
		void addSlotItem(Game::Part* part, Game::Slot* slot, QTreeWidgetItem* parent);
		void updatePlot();

		struct ItemSlot{
			Game::Part* part;
			Game::Slot* slot;
		};

		std::map<QTreeWidgetItem*, ItemSlot> itemSlots;
		Game::Vehicle* vehicle;
		std::unique_ptr<TypeSpecificForm> editor;
		MainWindow& mainWindow;
		TypeSpecificFormFactory& typeSpecificFormFactory;
		QStringListModel partTemplateList;
		PartTableModel partTableModel;

	private slots:
		void objectEdited();

		void on_openPerformanceFormButton_clicked();
		void on_createAndAttachPartButton_clicked();
		void on_partTable_clicked(const QModelIndex &index);
};

};

#endif
