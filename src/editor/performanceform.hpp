#ifndef EDITOR_PERFORMANCEFORM_HPP
#define EDITOR_PERFORMANCEFORM_HPP

#include <memory>

#include "typespecificform.hpp"
#include "ui_performanceform.h"
#include "game/vehicle.hpp"
#include <QTreeWidgetItem>

namespace Editor
{

class PerformanceForm : public QWidget, private Ui::PerformanceForm
{
	Q_OBJECT

	public:
		explicit PerformanceForm(Game::Vehicle* vehicle, QWidget* parent = 0);

	private:
		void updatePvPlot();
		void updatePerformancePlot();

		Game::Vehicle* vehicle;

	private slots:
		void paramsChanged();

};

};

#endif
