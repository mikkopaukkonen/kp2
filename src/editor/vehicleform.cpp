#include "vehicleform.hpp"
#include "typespecificformfactory.hpp"
#include "performanceform.hpp"
#include "deserializer.hpp"
#include "utils/directory.hpp"
#include "physics/vehicle.hpp"

#include <QVector>


namespace Editor
{
void VehicleForm::updatePlot()
{
	std::map<int, Physics::NewtonMeters> torqueData;
	std::map<int, Physics::Watts> powerData;

	try {
		Physics::Vehicle simulation(*vehicle);

		while(simulation.getState().time < 10.0)
		{
			const double PI = 3.14159265;
			const double RADS_TO_RPM = 60.0f / (2.0f * PI);

			int rpm = simulation.getState().engineSpeed * RADS_TO_RPM;

			torqueData[rpm] = simulation.getState().engineTorque;
			powerData[rpm] = simulation.getState().enginePower / 1000.0;

			simulation.advanceSimulation();
		}

		QVector<double> rpm;
		QVector<double> torque;
		QVector<double> power;

		for(auto pair : torqueData)
		{
			rpm.push_back(pair.first);
			torque.push_back(pair.second);

			std::cout << pair.first << " " << pair.second << std::endl;
		}

		for(auto pair : powerData)
		{
			power.push_back(pair.second);

			std::cout << pair.second << std::endl;
		}

		plot->clearGraphs();

		plot->addGraph();
		plot->graph(0)->setData(rpm, power);
		plot->graph(0)->setPen(QPen(QColor(255, 40, 40)));
		plot->graph(0)->rescaleValueAxis(false);

		plot->addGraph();
		plot->graph(1)->setData(rpm, torque);
		plot->graph(1)->setValueAxis(plot->yAxis2);
		plot->graph(1)->setPen(QPen(QColor(40, 40, 255)));
		plot->graph(1)->rescaleValueAxis(true);

		plot->xAxis->setLabel(trUtf8("Kierrosluku (kierrosta/min)"));
		plot->yAxis->setLabel(trUtf8("Teho (kW)"));
		plot->yAxis->setLabelColor(QColor(255, 40, 40));
		plot->yAxis2->setVisible(true);
		plot->yAxis2->setLabel(trUtf8("Vääntö (Nm)"));
		plot->yAxis2->setLabelColor(QColor(40, 40, 255));

		plot->xAxis->setRange(0, 10000);
		plot->replot();
	} catch(...) {

	}
}

void VehicleForm::updateOriginal()
{
	std::string name = nameField->text().toStdString();
	int price = 0;
	int year = yearField->value();
	std::string info = infoField->toPlainText().toStdString();
	std::string imageName;

	vehicle->applyPropertiesOf(Game::Vehicle(name, price, year, info, imageName));
}

void VehicleForm::setItemIcon(QTreeWidgetItem* item)
{
	ItemSlot itemSlot = itemSlots[item];

	// vehicle->getPartAttached(part, slot);
	const Game::Part* part = vehicle->getAttachedPart(*itemSlot.part, *itemSlot.slot);

	if(part)
		item->setIcon(0, QIcon(QPixmap("data/editor/icons/item.png")));
	else
		item->setIcon(0, QIcon(QPixmap("data/editor/icons/empty.png")));
}

template <class T, class V>
T& getPartOfType(const V& parts) {
	auto it = std::find_if(parts.begin(), parts.end(), [&](const std::unique_ptr<Game::Part>& ptr) {
		T* t = dynamic_cast<T*>(ptr.get());

		return t != nullptr;
	});

	if (it == parts.end()) {
		throw std::invalid_argument("No chassis in vehicle");
	}

	return *dynamic_cast<T*>(it->get());
}

QStringList getTemplateNames() {
	QStringList list;

	for(std::string className : readDirectory("data/editor/templates/"))
	{
		list << className.c_str();
	}

	return list;
}

VehicleForm::VehicleForm(Game::Vehicle* vehicle, MainWindow& mainWindow, TypeSpecificFormFactory& typeSpecificFormFactory, QWidget *parent) :
	TypeSpecificForm(parent),
	vehicle(vehicle),
        mainWindow(mainWindow),
        typeSpecificFormFactory(typeSpecificFormFactory),
        partTemplateList(getTemplateNames()),
        partTableModel(vehicle->getAttachedParts())
{
	setupUi(this);

	nameField->setText(vehicle->getName().c_str());
	yearField->setValue(vehicle->getYear());
	infoField->setPlainText(vehicle->getInfo().c_str());

	newPartTemplateSelector->setModel(&partTemplateList);
	partTable->setModel(&partTableModel);

	updatePlot();

	connect(nameField, SIGNAL(textChanged(QString)), this, SIGNAL(changed()));
	connect(yearField, SIGNAL(valueChanged(int)), this, SIGNAL(changed()));
	connect(infoField, SIGNAL(textChanged()), this, SIGNAL(changed()));
}

void VehicleForm::openEditor(Game::Object* object)
{
	editorContainer->removeWidget(editor.get());

	editor.reset(typeSpecificFormFactory.createFormFor(object));

	editorContainer->addWidget(editor.get());
	editor->show();

	connect(editor.get(), SIGNAL(changed()), this, SLOT(objectEdited()));
}

void VehicleForm::objectEdited()
{
	editor->updateOriginal();

	updatePlot();
}

void VehicleForm::on_openPerformanceFormButton_clicked()
{
	mainWindow.openChildWindow(new PerformanceForm(vehicle));
}

void VehicleForm::on_createAndAttachPartButton_clicked()
{
	std::string className = this->newPartTemplateSelector->currentText().toStdString();

	std::unique_ptr<Game::Part> part = Editor::Deserializer().createObject<Game::Part>(className);

	vehicle->attachPart(part);
}

void VehicleForm::on_partTable_clicked(const QModelIndex &index)
{
	Game::Part* part = partTableModel.getObject(index.row());

	if(part)
	{
		openEditor(part);
	}
}

}
