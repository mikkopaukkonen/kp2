#include "performanceform.hpp"
#include "typespecificformfactory.hpp"
#include "deserializer.hpp"
#include "utils/directory.hpp"
#include "physics/vehicle.hpp"

#include <QVector>
#include <iostream>

namespace Editor
{

void PerformanceForm::updatePvPlot()
{
	Physics::Vehicle simulation(*vehicle);

	const double PI = 3.14159265;
	const double RADS_TO_RPM = 60.0f / (2.0f * PI);

	while(simulation.getState().time < 10.0 && simulation.getState().engineSpeed * RADS_TO_RPM < speedSlider->value())
	{
		simulation.advanceSimulation();
	}

	QVector<double> pressure;
	QVector<double> volume;

	for(auto pair : simulation.getState().pvGraph)
	{
		volume.push_back(pair.first);
		pressure.push_back(pair.second);

		std::cout << pair.first << " " << pair.second << std::endl;
	}

	pvPlot->clearGraphs();
	pvPlot->clearPlottables();
	pvPlot->yAxis->setScaleType(QCPAxis::stLogarithmic);
	QCPCurve* pvcurve = new QCPCurve(pvPlot->xAxis, pvPlot->yAxis);
	pvcurve->setData(volume, pressure);
	pvcurve->rescaleAxes();

	pvPlot->xAxis->setLabel(trUtf8("Tilavuus"));
	pvPlot->yAxis->setLabel(trUtf8("Paine"));
	pvPlot->yAxis->setLabelColor(QColor(255, 40, 40));

	pvPlot->replot();
}

void PerformanceForm::updatePerformancePlot()
{
	std::map<int, Physics::NewtonMeters> torqueData;
	std::map<int, Physics::Watts> powerData;

	Physics::Vehicle simulation(*vehicle);

	while(simulation.getState().time < 10.0)
	{
		const double PI = 3.14159265;
		const double RADS_TO_RPM = 60.0f / (2.0f * PI);

		int rpm = simulation.getState().engineSpeed * RADS_TO_RPM;

		torqueData[rpm] = simulation.getState().engineTorque;
		powerData[rpm] = simulation.getState().enginePower / 1000.0;

		simulation.advanceSimulation();
	}

	QVector<double> rpm;
	QVector<double> torque;
	QVector<double> power;

	for(auto pair : torqueData)
	{
		rpm.push_back(pair.first);
		torque.push_back(pair.second);

		std::cout << pair.first << " " << pair.second << std::endl;
	}

	for(auto pair : powerData)
	{
		power.push_back(pair.second);

		std::cout << pair.second << std::endl;
	}

	performancePlot->clearGraphs();

	performancePlot->addGraph();
	performancePlot->graph(0)->setData(rpm, power);
	performancePlot->graph(0)->setPen(QPen(QColor(255, 40, 40)));
	performancePlot->graph(0)->rescaleValueAxis(false);

	performancePlot->addGraph();
	performancePlot->graph(1)->setData(rpm, torque);
	performancePlot->graph(1)->setValueAxis(performancePlot->yAxis2);
	performancePlot->graph(1)->setPen(QPen(QColor(40, 40, 255)));
	performancePlot->graph(1)->rescaleValueAxis(true);

	performancePlot->xAxis->setLabel(trUtf8("Kierrosluku (kierrosta/min)"));
	performancePlot->yAxis->setLabel(trUtf8("Teho (kW)"));
	performancePlot->yAxis->setLabelColor(QColor(255, 40, 40));
	performancePlot->yAxis2->setVisible(true);
	performancePlot->yAxis2->setLabel(trUtf8("Vääntö (Nm)"));
	performancePlot->yAxis2->setLabelColor(QColor(40, 40, 255));

	performancePlot->xAxis->setRange(0, 10000);
	performancePlot->replot();
}

PerformanceForm::PerformanceForm(Game::Vehicle* vehicle, QWidget *parent) :
	QWidget(parent),
	vehicle(vehicle)
{
	setupUi(this);

	updatePvPlot();
	updatePerformancePlot();

	connect(speedSlider, SIGNAL(valueChanged(int)), this, SLOT(paramsChanged()));
	connect(throttleSlider, SIGNAL(valueChanged(int)), this, SLOT(paramsChanged()));
}

void PerformanceForm::paramsChanged()
{
	updatePvPlot();
	updatePerformancePlot();

}

}
