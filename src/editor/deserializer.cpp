#include "deserializer.hpp"

#include "json/value.h"

#include <fstream>

namespace Editor
{

Json::Value Deserializer::getObjectTemplate(const std::string& className) {
	Json::Value objectTemplate;

	std::string fileName = "data/editor/templates/";
	fileName += className;

	std::ifstream(fileName) >> objectTemplate;

	return objectTemplate;
}

}
