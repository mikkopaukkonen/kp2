#include "crankshaftform.hpp"

#include <string>

namespace Editor
{

void CrankshaftForm::updateOriginal()
{
	double stroke = strokeField->value();
	double rod = rodField->value();

	crankshaft->applyPropertiesOf(Game::Crankshaft(stroke, rod));
}

CrankshaftForm::CrankshaftForm(Game::Crankshaft* crankshaft, QWidget *parent):
	TypeSpecificForm(parent),
	crankshaft(crankshaft)
{
	setupUi(this);

	strokeField->setValue(crankshaft->getStroke());
	rodField->setValue(crankshaft->getRod());

	connect(strokeField, SIGNAL(valueChanged(double)), this, SIGNAL(changed()));
	connect(rodField, SIGNAL(valueChanged(double)), this, SIGNAL(changed()));
}

}
