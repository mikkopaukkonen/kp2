#include "mainwindow.hpp"

namespace Client
{

void MainWindow::addMenu(Menu* menu)
{
	menuContainer->addMenu(menu);
}

void MainWindow::navigateTo(QString menu)
{
	menuContainer->navigateTo(menu);
}

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	setupUi(this);
}

}
