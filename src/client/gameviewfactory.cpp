#include "gameviewfactory.hpp"

#include "gameview.hpp"
#include "connection.hpp"
#include "deserializer.hpp"

#include "gamemainmenu.hpp"
#include "carshopmenu.hpp"
#include "garagemenu.hpp"

namespace Client
{

GameViewFactory::GameViewFactory(Client::MainWindow& mainWindow):
        mainWindow(mainWindow),
        gameView(nullptr)

{

}

void GameViewFactory::connectionStateLoaded(Game::ConnectionState& connectionState)
{
	using namespace std::placeholders;

	listener = connectionState.getSession().addListener(Game::Object::Listener(std::bind(&onChange, this, _1)));
	this->connectionState = &connectionState;
}

void GameViewFactory::onChange(Game::Object* object)
{
	Game::Session* session = dynamic_cast<Game::Session*>(object);

	assert(session != nullptr);
	assert(connectionState != nullptr);

	Game::ConnectionState& connectionState = *this->connectionState;

	if(!session->getPlayer() && this->gameView) {
		gameView->deleteLater();
		gameView = nullptr;
	} else if(session->getPlayer() && !this->gameView) {
		Client::GameView* gameView = new Client::GameView(connectionState);
		this->gameView = gameView;

		mainWindow.addMenu(gameView);
		mainWindow.navigateTo("GameView");

		Client::GameMainMenu* gameMainMenu = new Client::GameMainMenu(connectionState);
		Client::CarShopMenu* carShopMenu = new Client::CarShopMenu(connectionState);
		Client::GarageMenu* garageMenu = new Client::GarageMenu(connectionState);

		gameView->addMenu(gameMainMenu);
		gameView->addMenu(carShopMenu);
		gameView->addMenu(garageMenu);
	}
}

}
