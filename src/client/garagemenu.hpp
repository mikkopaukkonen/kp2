#ifndef CLIENT_GARAGEMENU_HPP
#define CLIENT_GARAGEMENU_HPP

#include <memory>

#include "gamemenu.hpp"
#include "ui_garagemenu.h"
#include "models/vehicletablemodel.hpp"

namespace Client
{

class GarageMenu : public GameMenu, private Ui::GarageMenu
{
	Q_OBJECT

	public:
		explicit GarageMenu(Game::ConnectionState& connectionState, QWidget *parent = 0);

	private:
		Game::ConnectionState& connectionState;

		std::unique_ptr<VehicleTableModel> model;

		Game::Vehicle* vehicle;

	private slots:
		void onCurrentChanged(const QModelIndex&, const QModelIndex&);
		void on_selectButton_clicked();
		void on_cancelButton_clicked();
};

}

#endif
