#ifndef IDENTIFICATION_HPP
#define IDENTIFICATION_HPP

#include <QSettings>

class Identification
{
	public:
		std::string getServerVisibleId(std::string serverId);

		Identification(QSettings& settings);

	private:
		QSettings& settings;

};

#endif // IDENTIFICATION_HPP
