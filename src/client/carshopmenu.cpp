#include "carshopmenu.hpp"

#include <QMessageBox>

namespace Client
{

CarShopMenu::CarShopMenu(Game::ConnectionState& connectionState, QWidget *parent) :
	GameMenu(connectionState, parent),
        connectionState(connectionState),
	model(std::make_unique<VehicleTableModel>(connectionState.getState().getShopVehicles())),
        vehicle(nullptr)
{
	setupUi(this);

	TableView* view = carList;

	view->setModel(model.get());
	connect(carList->selectionModel(), SIGNAL(currentChanged(QModelIndex, QModelIndex)), this, SLOT(onCurrentChanged(QModelIndex, QModelIndex)));

	view->showColumn(model->name.getIndex());
	view->showColumn(model->price.getIndex());

	view->horizontalHeader()->setSectionResizeMode(model->name.getIndex(), QHeaderView::Stretch);
}

void CarShopMenu::onCurrentChanged(const QModelIndex& current, const QModelIndex& previous)
{
	(void)previous;

	vehicle = model->getObject(current.row());

	if(!vehicle)
		return;

	carNameLabel->setText(vehicle->getName().c_str());
	carImage->setPixmap(QPixmap(QString("gamedata/vehicleimages/") + QString(vehicle->getImageName().c_str())));
	price->setText(QString::number(vehicle->getPrice()) + trUtf8(" e"));
	chassisMass->setText(QString::number(vehicle->getMass()) + QString(" kg"));
	engine->setText("1.0L S4 OVH\nTurbo");
	fuelintake->setText("2x30mm Kaasutin");
}

void CarShopMenu::on_cancelButton_clicked()
{
	navigateToPrevious();
}

void CarShopMenu::on_buyButton_clicked()
{
	if(!vehicle)
		return;

	try
	{
		connectionState.getSession().getPlayer()->buyVehicle(&connectionState.getState(), vehicle);

		navigateToPrevious();
		navigateTo("GarageMenu");
	}
	catch(Game::Player::InsufficientFundsException)
	{
		QMessageBox msgBox;
		msgBox.setText("Rahasi eivät riitä autoon.");
		msgBox.exec();
	}

}

}
