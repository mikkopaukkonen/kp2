#ifndef SETTINGSMENU_H
#define SETTINGSMENU_H

#include "sounds/musicplayer.hpp"
#include "sounds/soundplayer.hpp"
#include "menu.hpp"
#include "ui_settingsmenu.h"

namespace Client
{

class SettingsMenu : public Menu, private Ui::SettingsMenu
{
	Q_OBJECT

	public:
		explicit SettingsMenu(MusicPlayer& musicPlayer, SoundPlayer& soundPlayer, QWidget *parent = 0);

	private:
		MusicPlayer& musicPlayer;
		SoundPlayer& soundPlayer;

	private slots:
		void on_cancelButton_clicked();
		void on_musicVolumeSlider_valueChanged(int);
		void on_soundEffectVolumeSlider_valueChanged(int value);
};

}

#endif // SETTINGSMENU_H
