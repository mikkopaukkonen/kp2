#ifndef CLIENT_GAMEVIEWFACTORY_HPP
#define CLIENT_GAMEVIEWFACTORY_HPP

#include "menu.hpp"
#include "mainwindow.hpp"
#include "game/connectionstate.hpp"

namespace Client
{

class GameViewFactory: public Menu
{
	Q_OBJECT

	public:
		GameViewFactory(MainWindow& mainWindow);


	public slots:
		virtual void connectionStateLoaded(Game::ConnectionState& connectionState);


	private:
		void onChange(Game::Object*);

		Client::MainWindow& mainWindow;

		QWidget* gameView;
		Game::ConnectionState* connectionState;

		std::shared_ptr<Game::Object::Listener> listener;
};

}

#endif
