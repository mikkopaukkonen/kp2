#ifndef CLIENT_DESERIALIZER_HPP
#define CLIENT_DESERIALIZER_HPP

#include "game/defaultserializer.hpp"
#include "sounds/soundplayer.hpp"

namespace Client
{

class Connection;

class Deserializer : public Game::DefaultDeserializer
{
	public:
		Deserializer(Game::ObjectIdMapper& objectIdMapper, SoundPlayer& soundPlayer, Connection& connection);

	protected:
		virtual std::unique_ptr<Game::Serializable> instantiate(std::string type, const Json::Value &value);

	private:
		SoundPlayer& soundPlayer;
		Connection& connection;
};

}

#endif
