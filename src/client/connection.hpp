#ifndef CLIENT_CONNECTION_HPP
#define CLIENT_CONNECTION_HPP

#include <QObject>
#include <string>
#include <QProcess>
#include <QTcpSocket>

#include "net/packet.hpp"
#include "game/connectionstate.hpp"
#include "game/objectidmapper.hpp"
#include "deserializerfactory.hpp"
#include "deserializer.hpp"
#include "identification.hpp"

class Packet;

const int BUFFERSIZE=512;

namespace Client
{

class Connection : public QObject
{
	Q_OBJECT

	public:
		void connect(std::string hostname, int port, const std::string& playerName);
		void startLocalGame(const std::string& playerName);
		void continueLocalGame(const std::string& stateFileName);
		std::vector<std::string> readAvailableStateFileNames();

		void processPackets();

		Game::ConnectionState& getConnectionState();

		void writeToServer(const Net::Packet& packet);

		std::string getId(const Game::Object* object);
		void makeRemoteCall(const Game::Object* object, const std::string& method, const Json::Value& arguments);

		Connection(DeserializerFactory& deserializerFactory, Identification& identification);
		~Connection();

		void makeRemoteCall(const Game::Serializable& callObject);

	public slots:
		void close();

	signals:
		void startingLocalServer();
		void connectingToRemote();
		void connectingToLocal();
		void connected();
		void receivingGameState();
		void ready(Game::ConnectionState& connectionState);
		void error(const std::string& error);

	private:
		void startLocalServer(Json::Value settings);

		QTcpSocket socket;

		std::string receiveBuffer;
		char scrapBuffer[BUFFERSIZE];

		QProcess serverProcess;

		DeserializerFactory& deserializerFactory;

		Identification& identification;

		std::unique_ptr<Game::ObjectIdMapper> objectIdMapper;
		std::unique_ptr<Game::ConnectionState> connectionState;
		std::unique_ptr<Json::StreamWriter> jsonWriter;

		std::optional<std::string> preferredPlayerName;

	private slots:
		void onServerError(QProcess::ProcessError);
		void onServerStarted();
		void onConnected();
};

}

#endif
