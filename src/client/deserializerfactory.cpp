#include "deserializerfactory.hpp"

#include <string>

#include "connection.hpp"
#include "playerproxy.hpp"

namespace Client
{

std::unique_ptr<Deserializer> DeserializerFactory::construct(Game::ObjectIdMapper& objectIdMapper, Connection& connection)
{
	return std::make_unique<Client::Deserializer>(objectIdMapper, soundPlayer, connection);
}

DeserializerFactory::DeserializerFactory(SoundPlayer& soundPlayer):
	soundPlayer(soundPlayer)
{

}


}
