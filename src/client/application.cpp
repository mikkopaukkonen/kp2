#include "application.hpp"
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStyleFactory>

namespace Client
{

Application::Application(int argc, char** argv) :
	QApplication(argc, argv)
{
	applyCustomStyle();
}

void Application::applyCustomStyle()
{
    QApplication::setStyle(QStyleFactory::create("windows"));

	setStyleSheet(readStyleSheet());
}

QString Application::readStyleSheet()
{
	QFile file("data/style/stylesheet.qss");
	file.open(QIODevice::ReadOnly);
	QString styleSheet = QTextStream(&file).readAll();
	file.close();

	return styleSheet;
}

}
