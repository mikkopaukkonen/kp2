#include "connection.hpp"

#include <iostream>
#include <fstream>
#include <stdint.h>
#include <cstdlib>
#include <stdexcept>
#include <QDebug>

#include "json/json.h"
#include "utils/string.hpp"
#include "utils/directory.hpp"
#include "net/packet.hpp"
#include "net/clientsocket.hpp"
#include "protocol/protocol.hpp"
#include "game/defaultserializer.hpp"
#include "game/remotecall.hpp"
#include "game/objectidmapper.hpp"

namespace Client
{

const std::string SAVEGAME_DIRECTORY = "gamedata/saves/";

void Connection::connect(std::string hostname,int port, const std::string& playerName)
{
	preferredPlayerName = playerName;

	socket.connectToHost(hostname.c_str(), port);
}

void Connection::close()
{
	socket.close();

	serverProcess.waitForFinished();
}

void Connection::processPackets()
{
	if(socket.state() != QTcpSocket::ConnectedState)
		return;

	int read;

	while((read = socket.read(scrapBuffer, BUFFERSIZE)) > 0)
		receiveBuffer.append(scrapBuffer, read);

	if(read == -1)
		close();

	while(1)
	{
		Net::Packet packet;

		try
		{
			packet.readFromBuffer(receiveBuffer);

			if(packet.getType() == Protocol::GAME_STATE)
			{
				emit receivingGameState();

				std::string s;

				packet >> s;

				std::stringstream ss(s);

				Json::Value v;

				ss >> v;

				std::cout << "Got connection state: "<< std::endl <<  v << std::endl;

				try
				{
					objectIdMapper = std::make_unique<Game::ObjectIdMapper>("client:");
					auto deserializer = deserializerFactory.construct(*objectIdMapper, *this);
					connectionState = deserializer->deserializeExpectingType<Game::ConnectionState>(v);
					objectIdMapper->setRoot(connectionState.get());

					emit ready(*connectionState.get());

					std::string serverVisibleIdentifier = identification.getServerVisibleId(connectionState->getState().getIdentifier());
					std::clog << "Attempting to claim player with per-game identifier: " << serverVisibleIdentifier << std::endl;
					connectionState->getSession().claimPlayer(serverVisibleIdentifier);
				}
				catch(std::exception& e)
				{
					emit error(e.what());
				}
			}

			if(packet.getType() == Protocol::REMOTE_INVOCATION)
			{
				std::string s;
				packet >> s;
				std::stringstream ss(s);

				Json::Value call;
				ss >> call;

				std::cout << "Got remote invocation: "<< std::endl <<  call << std::endl;


				try {
					auto deserializer = deserializerFactory.construct(*objectIdMapper, *this);
					std::unique_ptr<Game::RemoteCall> remoteCall = deserializer->deserializeExpectingType<Game::RemoteCall>(call);

					remoteCall->callOnLocalObject();
				} catch (std::exception& e) {
					std::cerr << "Error while processing remote call " << e.what() <<  std::endl;
				}
			}

			if(connectionState && connectionState->getSession().getPlayer() && preferredPlayerName.has_value()) {
				connectionState->getSession().getPlayer()->setName(preferredPlayerName.value());
			}
		}
		catch(Net::EndOfDataException)
		{
			/*
			We don't have enough data for a packet, so we return.
			*/
			return;
		}

	}
}

Game::ConnectionState& Connection::getConnectionState()
{
	return *(connectionState.get());
}

void Connection::writeToServer(const Net::Packet& packet)
{
	std::string str = packet.getString();

	socket.write(str.c_str(), str.size());
}

void Connection::makeRemoteCall(const Game::Serializable& callObject)
{
	Game::DefaultSerializer serializer(*objectIdMapper);

	Json::Value call = serializer.serialize(callObject);

	Net::Packet packet;
	packet.setType(Protocol::REMOTE_INVOCATION);

	std::stringstream ss;

	jsonWriter->write(call, &ss);

	packet << ss.str();

	writeToServer(packet);

	std::cout << call;
}

void Connection::startLocalGame(const std::string& playerName)
{
	preferredPlayerName = playerName;

	Json::Value settings;

	startLocalServer(settings);
}

void Connection::continueLocalGame(const std::string& stateFileName)
{
	Json::Value settings;

	settings["stateFileName"] = SAVEGAME_DIRECTORY + stateFileName;

	startLocalServer(settings);
}

std::vector<std::string> Connection::readAvailableStateFileNames()
{
	return readDirectory(SAVEGAME_DIRECTORY, "kp2");
}

void Connection::startLocalServer(Json::Value settings)
{
	emit startingLocalServer();

	settings["port"] = 31000;
	settings["isLocal"] = true;
	settings["quitWhenEmpty"] = true;
	settings["connectionLimit"] = 1;

	std::ofstream("cfg/singleplayer.cfg") << settings;

	QString name = "kp2_server";
	QStringList args;
	args << "-config" << "cfg/singleplayer.cfg";

	serverProcess.setProcessChannelMode(QProcess::MergedChannels);
	serverProcess.setStandardOutputFile("server.log");

	serverProcess.start("./kp2_server", args);
}

void Connection::onServerError(QProcess::ProcessError error)
{
	std::cout << error << std::endl;
}

void Connection::onServerStarted()
{
	emit connectingToLocal();

	socket.connectToHost("localhost",31000);
}

void Connection::onConnected()
{
	emit connected();
}

std::unique_ptr<Json::StreamWriter> createJsonFastWriter() {
	Json::StreamWriterBuilder builder;
	builder["commentStyle"] = "None";
	builder["indentation"] = "";
	return std::unique_ptr<Json::StreamWriter>(builder.newStreamWriter());
}

Connection::Connection(DeserializerFactory& deserializerFactory, Identification& identification):
	deserializerFactory(deserializerFactory),
        identification(identification),
        jsonWriter(createJsonFastWriter())
{
	QObject::connect(&serverProcess, SIGNAL(started()), this, SLOT(onServerStarted()));
	QObject::connect(&serverProcess, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onServerError(QProcess::ProcessError)));
	QObject::connect(&socket, SIGNAL(connected()), this, SLOT(onConnected()));
}

Connection::~Connection()
{
	socket.close();

	serverProcess.waitForFinished();
}

}
