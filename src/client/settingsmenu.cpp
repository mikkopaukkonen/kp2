#include "settingsmenu.hpp"

namespace Client
{

SettingsMenu::SettingsMenu(MusicPlayer& musicPlayer, SoundPlayer& soundPlayer, QWidget *parent) :
	Menu(parent),
	musicPlayer(musicPlayer),
        soundPlayer(soundPlayer)
{
	setupUi(this);

	musicVolumeSlider->setValue(musicPlayer.getVolume());
	soundEffectVolumeSlider->setValue(soundPlayer.getVolume());
}

void SettingsMenu::on_cancelButton_clicked()
{
	navigateToPrevious();
}

void SettingsMenu::on_musicVolumeSlider_valueChanged(int volume)
{
	musicPlayer.setVolume(volume);
}

void SettingsMenu::on_soundEffectVolumeSlider_valueChanged(int volume)
{
	soundPlayer.setVolume(volume);

	soundPlayer.play("volume.wav");
}

}
