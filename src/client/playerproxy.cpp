#include "playerproxy.hpp"

#include "protocol/protocol.hpp"

#include "game/remotecall.hpp"

#include <iostream>

namespace Client
{

void PlayerProxy::setName(const std::string& name)
{
	Player::setName(name);

	connection.makeRemoteCall(Game::PlayerSetNameRemoteCall(this, name));
}

void PlayerProxy::setActiveVehicle(Game::Vehicle* vehicle)
{
	Player::setActiveVehicle(vehicle);

	connection.makeRemoteCall(Game::PlayerSetActiveVehicleRemoteCall(this, vehicle));
}

void PlayerProxy::buyPart(const Game::State* state, const Game::Part* part)
{
	std::cout << "PlayerProxy::buyPart: " << part << std::endl;

	try{
		Player::buyPart(state, part);

		soundPlayer.play("buy.wav");
	} catch (Player::InsufficientFundsException e) {
		soundPlayer.play("nofunds.wav");

		throw e;
	}

	connection.makeRemoteCall(Game::PlayerBuyPartRemoteCall(this, state, part));
}

void PlayerProxy::buyVehicle(const Game::State* state, const Game::Vehicle* shopVehicle)
{
	std::cout << "PlayerProxy::buyVehicle: " << shopVehicle << std::endl;

	try{
		Player::buyVehicle(state, shopVehicle);

		soundPlayer.play("buy.wav");
	} catch (Player::InsufficientFundsException e) {
		soundPlayer.play("nofunds.wav");

		throw e;
	}

	connection.makeRemoteCall(Game::PlayerBuyVehicleRemoteCall(this, state, shopVehicle));
}

PlayerProxy::PlayerProxy(const Json::Value& value, Deserializer& deserializer, Connection& connection, SoundPlayer& soundPlayer):
	Player(value, deserializer),
	connection(connection),
        soundPlayer(soundPlayer)
{

}

}
