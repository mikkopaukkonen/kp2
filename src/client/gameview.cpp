#include "gameview.hpp"

#include <sstream>
#include <cassert>
#include <functional>

namespace Client
{

void GameView::addMenu(GameMenu* menu)
{
	menuContainer->addMenu(menu);
}

void GameView::onChange(Game::Object* object)
{
	Game::Player* player = dynamic_cast<Game::Player*>(object);

	assert(player);

	update(player);
}

void GameView::on_settingsButton_clicked()
{
	emit navigateTo("SettingsMenu");
}

void GameView::update(Game::Player* player)
{
	std::stringstream s;

	s << player->getName() << ", ";
	s << player->getMoney();

	infoLabel->setText(s.str().c_str());
}

GameView::GameView(Game::ConnectionState& connectionState, QWidget *parent) :
	Menu(parent)
{
	setupUi(this);

	using namespace std::placeholders;

	Game::Player* player = connectionState.getSession().getPlayer();

	connection = player->addListener(Game::Object::Listener(std::bind(&onChange, this, _1)));

	update(player);
}

}

