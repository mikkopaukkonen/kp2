#ifndef CLIENT_DESERIALIZERFACTORY_HPP
#define CLIENT_DESERIALIZERFACTORY_HPP

#include "deserializer.hpp"
#include "sounds/soundplayer.hpp"

namespace Client
{

class Connection;

class DeserializerFactory
{
	public:
		virtual std::unique_ptr<Deserializer> construct(Game::ObjectIdMapper& objectIdMapper, Connection& connection);

		DeserializerFactory(SoundPlayer& soundPlayer);

	private:
		SoundPlayer& soundPlayer;
};

}

#endif
