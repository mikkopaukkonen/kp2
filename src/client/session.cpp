#include "session.hpp"

#include "game/remotecall.hpp"

namespace Client {

void Session::claimPlayer(std::string identifier)
{
	connection.makeRemoteCall(Game::SessionClaimPlayerRemoteCall(this, identifier));
}

Session::Session(const Json::Value serialized, Client::Deserializer& deserializer, Client::Connection& connection):
        Game::Session(serialized, deserializer),
	connection(connection)
{

}

}
