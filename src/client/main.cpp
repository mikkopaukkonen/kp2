#include "application.hpp"

#include <memory>
#include <QApplication>
#include <QStackedLayout>
#include <QString>
#include <QTextStream>
#include <QFile>
#include <QTimer>
#include <QtGlobal>

#include "identification.hpp"

#include "mainwindow.hpp"
#include "menu.hpp"
#include "updater.hpp"

#include "sounds/musicplayer.hpp"
#include "sounds/soundplayer.hpp"
#include "utils/outputredirector.hpp"

#include "mainmenu.hpp"
#include "settingsmenu.hpp"
#include "singleplayermenu.hpp"
#include "multiplayermenu.hpp"
#include "gameloadingscreen.hpp"
#include "connection.hpp"
#include "deserializer.hpp"

#include "gameviewfactory.hpp"


void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	QString txt;
	switch (type)
	{
		case QtInfoMsg:
			txt = QString("Info: %1 (%2:%3, %4)").arg(msg).arg(context.file).arg(context.line).arg(context.function);
			break;
		case QtDebugMsg:
			txt = QString("Debug: %1 (%2:%3, %4)").arg(msg).arg(context.file).arg(context.line).arg(context.function);
			break;
		case QtWarningMsg:
			txt = QString("Warning: %1 (%2:%3, %4)").arg(msg).arg(context.file).arg(context.line).arg(context.function);
			break;
		case QtCriticalMsg:
			txt = QString("Critical: %1 (%2:%3, %4)").arg(msg).arg(context.file).arg(context.line).arg(context.function);
			break;
		case QtFatalMsg:
			txt = QString("Fatal: %1 (%2:%3, %4)").arg(msg).arg(context.file).arg(context.line).arg(context.function);
			abort();
	}

    std::cerr << txt.toStdString() << std::endl;
}

int main(int argc, char *argv[])
{
    //OutputRedirector redirect("client.log");

	Client::Application application(argc, argv);
	application.setApplicationName("Kiihdytyspeli 2");
	application.setOrganizationName("Mikko Paukkonen");

	QSettings settings;
	Identification identification(settings);

	Client::MainWindow mainWindow;

	MusicPlayer musicPlayer;
	SoundPlayer soundPlayer;

	Client::DeserializerFactory deserializerFactory(soundPlayer);
	Client::Connection connection(deserializerFactory, identification);

	Client::MainMenu* mainMenu = new Client::MainMenu();
	Client::SettingsMenu* settingsMenu = new Client::SettingsMenu(musicPlayer, soundPlayer);
	Client::SinglePlayerMenu* singlePlayerMenu = new Client::SinglePlayerMenu(connection);
	Client::MultiPlayerMenu* multiPlayerMenu = new Client::MultiPlayerMenu(connection);
	Client::GameLoadingScreen* gameLoadingScreen = new Client::GameLoadingScreen(connection);

	Client::GameViewFactory gameViewFactory(mainWindow);

	QObject::connect(&connection, SIGNAL(ready(Game::ConnectionState&)), &gameViewFactory, SLOT(connectionStateLoaded(Game::ConnectionState&)));

	mainWindow.addMenu(mainMenu);
	mainWindow.addMenu(settingsMenu);
	mainWindow.addMenu(singlePlayerMenu);
	mainWindow.addMenu(multiPlayerMenu);
	mainWindow.addMenu(gameLoadingScreen);

	Client::Updater updater(musicPlayer, connection);

	QTimer updateTimer;
	QObject::connect(&updateTimer, SIGNAL(timeout()), &updater, SLOT(update()));

	updateTimer.start(500);

    qInstallMessageHandler(customMessageHandler);

	mainWindow.show();
	mainWindow.dumpObjectTree();

	return application.exec();
}
