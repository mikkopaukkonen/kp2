#include "singleplayermenu.hpp"
#include "ui_singleplayermenu.h"

namespace Client
{

QStringList getStateFileNames(Connection& connection) {
	QStringList list;

	for(std::string className : connection.readAvailableStateFileNames())
	{
		list << className.c_str();
	}

	return list;
}

SinglePlayerMenu::SinglePlayerMenu(Connection& connection, QWidget *parent) :
	Menu(parent),
	connection(connection),
        stateFileNameListModel(getStateFileNames(connection))
{
	setupUi(this);

	stateFileList->setModel(&stateFileNameListModel);
}

void SinglePlayerMenu::on_startGameButton_clicked()
{
	connection.startLocalGame(playerNameInput->text().toStdString());

	navigateTo("GameLoadingScreen");
}

void SinglePlayerMenu::on_cancelButton_clicked()
{
	navigateToPrevious();
}

void SinglePlayerMenu::on_loadGameButton_clicked()
{
	std::string stateFileName = stateFileNameListModel.data(stateFileList->currentIndex()).toString().toStdString();

	connection.continueLocalGame(stateFileName);

	navigateTo("GameLoadingScreen");
}

}
