#ifndef CLIENT_SESSION_HPP
#define CLIENT_SESSION_HPP

#include "game/session.hpp"

#include "connection.hpp"

namespace Client {

class Session: public Game::Session
{
	public:
		virtual void claimPlayer(std::string identifier);

		Session(const Json::Value serialized, Deserializer& deserializer, Connection& connection);

	private:
		Connection& connection;
};

}

#endif // SESSION_HPP
