#ifndef CLIENT_GAMEMENU_HPP
#define CLIENT_GAMEMENU_HPP

#include "menu.hpp"
#include "game/connectionstate.hpp"

namespace Client
{

class GameMenu: public Menu
{
	Q_OBJECT

	public:
		explicit GameMenu(Game::ConnectionState& connectionState, QWidget *parent = 0);

};

}

#endif
