#ifndef CLIENT_GAMEVIEW_H
#define CLIENT_GAMEVIEW_H

#include <memory>

#include <QWidget>

#include "ui_gameview.h"

#include "menu.hpp"
#include "gamemenu.hpp"

namespace Client
{

class GameView : public Menu, private Ui::GameView
{
	Q_OBJECT

	public:
		void addMenu(GameMenu *menu);
		void onChange(Game::Object* object);

		explicit GameView(Game::ConnectionState& connectionState, QWidget *parent = 0);

	private:
		std::shared_ptr<Game::Object::Listener> connection;

	private slots:
		void on_settingsButton_clicked();
		void update(Game::Player* player);
};

}

#endif
