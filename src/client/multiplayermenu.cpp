#include "multiplayermenu.hpp"

namespace Client
{

MultiPlayerMenu::MultiPlayerMenu(Connection& connection, QWidget *parent) :
	Menu(parent),
	connection(connection)
{
	setupUi(this);
}

void MultiPlayerMenu::on_connectButton_clicked()
{
	connection.connect("localhost", 31001, playerNameInput->text().toStdString());
}

void MultiPlayerMenu::on_cancelButton_clicked()
{
	navigateToPrevious();
}

}
