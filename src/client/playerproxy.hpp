#ifndef CLIENT_PLAYERPROXY_HPP
#define CLIENT_PLAYERPROXY_HPP

#include "game/player.hpp"
#include "connection.hpp"
#include "sounds/soundplayer.hpp"

namespace Client
{

class InvalidPlayerException
{

};

class PlayerProxy : public Game::Player
{
	public:
		virtual void setName(const std::string& name);
		virtual void setActiveVehicle(Game::Vehicle* vehicle);

		virtual void buyPart(const Game::State* state, const Game::Part* part);
		virtual void buyVehicle(const Game::State* state, const Game::Vehicle* vehicle);

		PlayerProxy(const Json::Value& value, Deserializer& deserializer, Connection& connection, SoundPlayer& soundPlayer);

	private:
		int getIndex();

		Connection& connection;
		SoundPlayer& soundPlayer;
};

}

#endif
