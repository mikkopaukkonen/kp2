#include "deserializer.hpp"

#include <string>
#include <memory>

#include "connection.hpp"
#include "playerproxy.hpp"
#include "session.hpp"

namespace Client
{

Deserializer::Deserializer(Game::ObjectIdMapper& objectIdMapper, SoundPlayer& soundPlayer, Connection& connection):
	Game::DefaultDeserializer(objectIdMapper),
	soundPlayer(soundPlayer),
	connection(connection)
{

}

std::unique_ptr<Game::Serializable> Deserializer::instantiate(std::string type, const Json::Value& value)
{
	if(type == "Player")
		return std::make_unique<PlayerProxy>(value, *this, connection, soundPlayer);
	if(type == "Session")
		return std::make_unique<Session>(value, *this, connection);

	return Game::DefaultDeserializer::instantiate(type, value);
}

}
