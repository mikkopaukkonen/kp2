#include "identification.hpp"

#include "utils/identifier.hpp"

#include <cryptopp/cryptlib.h>
#include <cryptopp/hex.h>
#include <cryptopp/sha.h>

#include <stdexcept>

std::string ensureIdExists(QSettings& settings) {
	if (!settings.contains("clientId")) {
		settings.setValue("clientId", QString(generateIdentifier().c_str()));
	}

	return settings.value("clientId").toString().toStdString();
}

std::string Identification::getServerVisibleId(std::string serverId)
{
	if(serverId.length() < 32) {
		throw std::invalid_argument("serverId must be at least 32 chars");
	}

	using namespace CryptoPP;

	std::string clientId = ensureIdExists(settings);

	SHA256 hash;
	std::string message = clientId + serverId;
	std::string digest;

	StringSource(message, true, new HexDecoder(new HashFilter(hash, new HexEncoder(new StringSink(digest)))));

	return digest;
}

Identification::Identification(QSettings& settings):
        settings(settings)
{

}
